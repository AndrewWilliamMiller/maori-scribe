﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Maori
{
    [TestFixture]
    public class NewPlayModeTest
    {
        internal Scene TestScene;
        private string TestSceneName = "Start";
        [OneTimeSetUp]
        public void Initialise()
        {
        }

        [UnityTest]
        public IEnumerator TestAmountOfBlanksNotEmpty()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            // Assert
            string blanks = GameObject.FindGameObjectWithTag("SelectedBlankSetting").GetComponent<Text>().text;
            Assert.IsTrue(!string.IsNullOrEmpty(blanks));
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestHideOptions()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            StartController StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            GameObject StartOptions = GameObject.FindGameObjectWithTag("StartOptions");
            yield return null;
            // Action
            StartController.LaunchHelp();
            // Assert
            Assert.IsTrue(StartOptions.activeSelf == false);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestHelpLeftNavDisabledOnPage1()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            StartController StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            yield return null;
            //Action
            StartController.LaunchHelp();
            yield return null;
            Manual manual = GameObject.FindGameObjectWithTag("Manual").GetComponent<Manual>();
            manual.LoadPageFromTableOfContents("Page1");
            yield return null;
            Button previousHelpBtn = GameObject.FindGameObjectWithTag("PreviousHelpBtn").GetComponent<Button>();
            // Assert
            Assert.IsTrue(previousHelpBtn.interactable == false);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestBothNavButtonsEnabled2To9()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            StartController StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            yield return null;
            StartController.LaunchHelp();
            yield return null;
            Manual manual = GameObject.FindGameObjectWithTag("Manual").GetComponent<Manual>();
            manual.LoadPageFromTableOfContents("Page1");
            Button previousHelpBtn = GameObject.FindGameObjectWithTag("PreviousHelpBtn").GetComponent<Button>();
            Button nextHelpBtn = GameObject.FindGameObjectWithTag("NextHelpBtn").GetComponent<Button>();
            // Action
            bool result = true;
            for (int i = 2; i < 10; i++)
            {
                manual.LoadPageFromTableOfContents("Page" + i);
                yield return null;
                if(previousHelpBtn.interactable != true || nextHelpBtn.interactable != true)
                {
                    result = false;
                    Debug.Log("Broke on page: " + i + ". Previous Help Button interactivity is: " + previousHelpBtn.interactable +
                        " and Next Help Button interactivity is: " + nextHelpBtn.interactable);
                    break;
                }
            }
            // Assert
            Assert.IsTrue(result);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);

        }

        [UnityTest]
        public IEnumerator Test5FloatingWords()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            // Get past maximum delay on floating word spawn times.
            yield return new WaitForSeconds(5.25f);
            int expected = 5;
            // Action
            int actual = GameObject.FindGameObjectsWithTag("FloatingWord").Length;
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestStopFloatingWords()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            FloatingTileManipulator floatingTileManipulator = GameObject.FindGameObjectWithTag("FloatingTileProjection").GetComponent<FloatingTileManipulator>();
            // Get past maximum delay on floating word spawn times.
            yield return new WaitForSeconds(5.25f);
            floatingTileManipulator.StopAll();
            yield return null;
            int expected = 0;
            // Action
            int actual = GameObject.FindGameObjectsWithTag("FloatingWord").Length;
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestLoadHelpPageImage()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            string pageToLoad = "page6";
            StartController StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            yield return null;
            StartController.LaunchHelp();
            yield return null;
            Manual manual = GameObject.FindGameObjectWithTag("Manual").GetComponent<Manual>();
            // Action
            manual.LoadPageFromTableOfContents(pageToLoad);
            Image helpPageImage = GameObject.FindGameObjectWithTag("ManualPage").GetComponent<Image>();
            string actual = helpPageImage.sprite.name;
            // Assert
            Debug.Log("Page to Load: " + pageToLoad + " Page Loaded: " + actual);
            Assert.AreEqual(pageToLoad, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        // Called before a test so that objects from the scene can be accessed, otherwise a test scene is all there is (an empty scene that is not one of the games scenes)
        public IEnumerator InitialiseScene(string sceneName)
        {
            TestScene = SceneManager.GetActiveScene();
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        }

        // Clean up the scene that was used in the test.
        public IEnumerator SceneCleanUp(string currentScene)
        {
            SceneManager.SetActiveScene(TestScene);
            yield return SceneManager.UnloadSceneAsync(currentScene);
        }
    }
}

