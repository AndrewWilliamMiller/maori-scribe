﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

namespace Maori
{
    public class MainSceneTest
    {
        internal Scene TestScene;
        private string TestSceneName = "Main";

        [UnityTest]
        public IEnumerator TestNameChangeUpdateInController()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            string newName = "Carl";
            Controller Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            yield return null;
            Controller.EditName();
            yield return null;
            InputField nameInput = GameObject.FindGameObjectWithTag("NewNameInputField").GetComponent<InputField>();
            nameInput.text = newName;
            // Action - name handling logic occurs when focus is lost.
            nameInput.DeactivateInputField();
            yield return new WaitForSeconds(1);
            Debug.Log("Input focused: " + nameInput.isFocused);
            // Assert
            Assert.AreEqual(newName, Controller.GetFormattedPlayerNames()[0]);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestClearWordLookUp()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            InputField searchWordField = GameObject.FindGameObjectWithTag("SearchWordField").GetComponent<InputField>();
            searchWordField.text = "Test";
            Controller Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            // Action
            Controller.ClearWord();
            // Asert
            Assert.AreEqual("", searchWordField.text);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestStarCellIs2WBonus()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            BoardScanner BoardScanner = GameObject.FindGameObjectWithTag("Controller").GetComponent<BoardScanner>();
            string starRowCell = "Row7,Cell7";
            // Assert
            CellBonusType expected = CellBonusType.Cell2W;
            CellBonusType actual = BoardScanner.GetBonusType(starRowCell);
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestCornerCellsAre3WBonus()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            BoardScanner BoardScanner = GameObject.FindGameObjectWithTag("Controller").GetComponent<BoardScanner>();
            string[] cornerCells = new[] { "Row0,Cell0", "Row0,Cell14", "Row14,Cell0", "Row14,Cell14" };
            bool result = true;
            // Action
            foreach (string rowCell in cornerCells)
            {
                if (!(BoardScanner.GetBonusType(rowCell) == CellBonusType.Cell3W))
                {
                    result = false;
                    break;
                }
            }
            // Assert
            Assert.True(result);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestBlankTileSelectionInitialise()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            BlankTileSelection blankTileSelection = GameObject.FindGameObjectWithTag("BlankSelectionSection").GetComponent<BlankTileSelection>();
            yield return null;
            blankTileSelection.Show();
            yield return null;
            GameObject BlankSelectionSection = GameObject.FindGameObjectWithTag("BlankSelectionSection");
            // Assert
            Assert.IsNotNull(BlankSelectionSection);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestCentre2LBonusCells()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            BoardScanner BoardScanner = GameObject.FindGameObjectWithTag("Controller").GetComponent<BoardScanner>();
            string[] centreCells = new[] { "Row6,Cell6", "Row6,Cell8", "Row8,Cell6", "Row8,Cell6" };
            bool result = true;
            // Action
            foreach (string rowCell in centreCells)
            {
                if (!(BoardScanner.GetBonusType(rowCell) == CellBonusType.Cell2L))
                {
                    result = false;
                    break;
                }
            }
            // Assert
            Assert.True(result);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestPanelSwitch()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            // Wait for startup panel to fade in.
            yield return new WaitForSeconds(3);
            CommandPanelController commandPanelController = GameObject.FindGameObjectWithTag("Controller").GetComponent<CommandPanelController>();
            string panelToLoad = "SwapTile";
            // Action
            commandPanelController.SwitchPanel(panelToLoad);
            yield return new WaitForSeconds(3);
            GameObject swapTileOptions = GameObject.FindGameObjectWithTag("SwapTileOptions");
            // Assert
            Assert.IsNotNull(swapTileOptions);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestPointDisplayUpdate()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            InformationDisplay informationDisplay = GameObject.FindGameObjectWithTag("InformationDisplay").GetComponent<InformationDisplay>();
            int points = 25;
            // Action
            informationDisplay.SetPlayerPoints("Player1", points);
            Text player1PointsDisplay = GameObject.FindGameObjectWithTag("Player1PointValue").GetComponent<Text>();
            int actual = Convert.ToInt32(player1PointsDisplay.text);
            // Assert
            Assert.AreEqual(points, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestFormattedNameDisplayUpdate()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            InformationDisplay informationDisplay = GameObject.FindGameObjectWithTag("InformationDisplay").GetComponent<InformationDisplay>();
            // Action
            informationDisplay.SetCurrentPlayer("Player2");
            // Assert
            string expected = "Player Two";
            string actual = GameObject.FindGameObjectWithTag("CurrentPlayer").GetComponent<Text>().text;
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestFeedbackDisplayUpdate()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            InformationDisplay informationDisplay = GameObject.FindGameObjectWithTag("InformationDisplay").GetComponent<InformationDisplay>();
            string feedback = "Test Feedback";
            // Action
            informationDisplay.SetFeedbackStatement(feedback);
            // Assert
            string actual = GameObject.FindGameObjectWithTag("FeedbackStatement").GetComponent<Text>().text;
            Assert.AreEqual(feedback, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestDoubleFeedbackDisplay()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            Controller controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            string feedback1 = "Test Feedback";
            string feedback2 = "More Information";
            // Action
            controller.GiveFeedback(feedback1);
            yield return null;
            controller.GiveFeedback(feedback2);
            yield return null;
            // Assert
            string actual = GameObject.FindGameObjectWithTag("FeedbackStatement").GetComponent<Text>().text;
            Debug.Log("message: " + actual);
            Assert.IsTrue(actual.Contains(feedback1) && actual.Contains(feedback2));
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestFeedbackClearedAfterSeconds()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            // Get past initialisation of scene which uses the feedback
            yield return new WaitForSeconds(4f);
            Controller controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            // purge initialisation feedback text
            string feedback = "Test Feedback";
            float delay = 1.0f;
            // Action
            controller.GiveFeedback(feedback, delay);
            yield return new WaitForSeconds(delay * 1.25f);
            // Assert
            string expected = "";
            string actual = GameObject.FindGameObjectWithTag("FeedbackStatement").GetComponent<Text>().text;
            Debug.Log("actual: " + actual);
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [UnityTest]
        public IEnumerator TestClearFeedback()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return null;
            InformationDisplay informationDisplay = GameObject.FindGameObjectWithTag("InformationDisplay").GetComponent<InformationDisplay>();
            string feedback = "Test Feedback";
            informationDisplay.SetFeedbackStatement(feedback);
            // Action
            informationDisplay.ClearFeedback();
            // Assert
            string expected = "";
            string actual = GameObject.FindGameObjectWithTag("FeedbackStatement").GetComponent<Text>().text;
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        [Test]
        public void TestRowCellIntConversion()
        {
            // Setup
            GameObject emptyObject = new GameObject();
            emptyObject.AddComponent<PlaceableCellCalculator>();
            PlaceableCellCalculator placeableCellCalculator = emptyObject.GetComponent<PlaceableCellCalculator>();
            string rowCellToConvert = "Row5,Cell7";
            int[] expected = new int[] { 5, 7 };
            int[] actual = placeableCellCalculator.RowCellToNums(rowCellToConvert);
            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestTileLetterSwitch()
        {
            // Setup
            Tile tile = new Tile("WH", 5);
            tile.SwitchLetter("A", 1);
            string expected = "A1";
            string actual = tile.Letter + tile.PointValue;
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [UnityTest]
        public IEnumerator Test7TileHolderTilesOnLaunch()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            // wait for tiles to spawn
            yield return new WaitForSeconds(2f);
            GameObject[] scannedGameObjects = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
            int expected = 7;
            int actual = 0;
            // Action
            foreach (GameObject gameObject in scannedGameObjects)
            {
                if (gameObject.name.Equals("TileHolderTile(Clone)"))
                    actual++;
            }
            // Assert
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        // Tests the Ultra High ratio as this is the default if the scene can't grab the setting from start menu
        [UnityTest]
        public IEnumerator Test225TotalTilesInTileRatios()
        {
            // Setup
            yield return InitialiseScene(TestSceneName);
            yield return new WaitForSeconds(0.1f);
            GameObject empty = new GameObject();
            empty.AddComponent<TileRepository>();
            TileRepository tileRepository = empty.GetComponent<TileRepository>();
            // Action
            int[] tileRatio = tileRepository.GetLetterRatio();
            int actual = 0;
            foreach (int tileCount in tileRatio)
            {
                actual += tileCount;
            }
            int expected = 225;
            // Assert
            Assert.AreEqual(expected, actual);
            // Cleanup
            yield return SceneCleanUp(TestSceneName);
        }

        // Called before a test so that objects from the scene can be accessed
        // otherwise a test scene is all there is (an empty scene that is not one of the games scenes)
        public IEnumerator InitialiseScene(string sceneName)
        {
            TestScene = SceneManager.GetActiveScene();
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        }

        // Clean up the scene that was used in the test.
        public IEnumerator SceneCleanUp(string currentScene)
        {
            SceneManager.SetActiveScene(TestScene);
            yield return SceneManager.UnloadSceneAsync(currentScene);
        }
    }
}

