﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Maori
{
    // byte values in this enum represent the multipliers, same valued enums cause several issues
    // so word multipliers are stored as higher values and divided by 10 to get their original value
    enum CellBonusType : byte { Cell, Cell2L = 2, Cell3L = 3, Cell2W = 20, Cell3W = 30 }
    public class Controller : MonoBehaviour
    {
        private AudioSource BtnSoundEffect;
        DateTime LastMsgUpdateTime = DateTime.Now;
        private Coroutine currentClearRoutine = null;
        private int EndGamePassCount;
        private int NumPlayers;
        private int ConsecutivePasses = 0;
        internal Settings Settings = null;
        internal PointTally PointTally;
        internal string[] Players;
        CommandPanelController CommandPanelController;
        internal Selector Selector;
        internal TilePlacementOptions TilePlacementOptions;
        internal SwapTileOptions SwapTileOptions;
        internal TileHolderSpawner TileHolderSpawner;
        internal UndoManager UndoManager;
        internal InformationDisplay InformationDisplay;
        internal PrimaryOptions PrimaryOptions;
        internal string PanelThisTurn = "";
        internal PlaceableCellCalculator PlaceableCellCalculator;
        internal TileRepository TileRepository;
        internal BoardScanner BoardScanner;
        internal BlankTileSelection BlankTileSelection;
        internal bool HasSwappedThisTurn = false;
        internal bool GameFinished = false;
        internal BoardTileSpawner BoardTileSpawner;
        internal WordLookUp WordLookUp;
        internal string GameName = "Maori Scribe";
        string[] FormattedPlayerNames = { "Player One", "Player Two", "Player Three", "Player Four" };

        private string currentPlayer = "Player1";

        public string CurrentPlayer
        {
            get { return currentPlayer; }
            set
            {
                currentPlayer = value;
                InformationDisplay.SetCurrentPlayer(value);
            }
        }

        internal void Undo()
        {
            UndoManager.Undo();
            PlaceableCellCalculator.Undo();
            PointTally.Undo();
            Selector.ClearTilePlacementHighlight();
        }

        internal void EditName()
        {
            InformationDisplay.InitialiseUpdateName();
        }

        internal void ClearWord()
        {
            WordLookUp.ClearWord();
        }

        internal void SearchWord()
        {
            WordLookUp.SearchWord();
        }

        internal void RemoveBlankTileToConvert(GameObject boardTile)
        {
            BoardTileSpawner.RemoveBlankTileToConvert(boardTile);
        }

        internal void ScoreConnectedWord(string[] rowCells, string playedRowCell)
        {
            CellBonusType bonusOfPlayedCell = BoardScanner.GetBonusType(playedRowCell);
            int playedLetterValue = BoardScanner.GetTileAtRowCell(playedRowCell).PointValue;
            int[] letterValues = new int[rowCells.Length];
            for (int i = 0; i < rowCells.Length; i++)
            {
                letterValues[i] = BoardScanner.GetTileAtRowCell(rowCells[i]).PointValue;
            }
            PointTally.ScoreConnectedWord(letterValues, bonusOfPlayedCell, playedLetterValue);
        }

        internal void Grant7TileBonus()
        {
            PointTally.Grant7TileBonus();
        }

        private void Awake()
        {
            Initialise();
        }
        internal void Initialise()
        {
            try
            {
                Settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<Settings>();
            }
            catch
            {
                Debug.Log("Trying to launch main scene without settings object");
            }
            BtnSoundEffect = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
            LoadSettings();
            CommandPanelController = GetComponent<CommandPanelController>();
            TileHolderSpawner = GameObject.FindGameObjectWithTag("TileHolderSpawnPoints").GetComponent<TileHolderSpawner>();
            PointTally = GetComponent<PointTally>();
            Selector = GetComponent<Selector>();
            TilePlacementOptions = GameObject.FindGameObjectWithTag("TilePlacementOptions").GetComponent<TilePlacementOptions>();
            SwapTileOptions = GameObject.FindGameObjectWithTag("SwapTileOptions").GetComponent<SwapTileOptions>();
            UndoManager = GetComponent<UndoManager>();
            InformationDisplay = GameObject.FindGameObjectWithTag("InformationDisplay").GetComponent<InformationDisplay>();
            PrimaryOptions = GameObject.Find("PrimaryOptions").GetComponent<PrimaryOptions>();
            PlaceableCellCalculator = GetComponent<PlaceableCellCalculator>();
            TileRepository = GetComponent<TileRepository>();
            BoardScanner = GetComponent<BoardScanner>();
            BlankTileSelection = GameObject.FindGameObjectWithTag("BlankSelectionSection").GetComponent<BlankTileSelection>();
            BoardTileSpawner = GameObject.FindGameObjectWithTag("GridArray").GetComponent<BoardTileSpawner>();
            WordLookUp = GameObject.FindGameObjectWithTag("WordLookUpSection").GetComponent<WordLookUp>();
        }

        internal void PlayButtonClickSound()
        {
            // allows for launching of main scene where a settings object won't be instantiated.
            if (Settings != null && Settings.SoundEffectsEnabled)
                BtnSoundEffect.Play();
        }

        private void LoadSettings()
        {
            LoadPlayerCount();
            EndGamePassCount = NumPlayers * 2;
        }



        private void LoadPlayerCount()
        {
            NumPlayers = 4;
            if (Settings != null)
                NumPlayers = Settings.NumPlayers;
            InitialisePlayers();
        }

        internal int GetNumPlayers()
        {
            return NumPlayers;
        }

        private IEnumerator ClearFeedback(float delay)
        {
            yield return new WaitForSeconds(delay);
            InformationDisplay.ClearFeedback();
        }

        internal void GiveFeedbackRemainingTilesForSwap()
        {
            int remainingTiles = TileRepository.GetRemainingTiles();
            if (remainingTiles == 1)
            {
                GiveFeedback("There is only 1 tile left to swap with");
            }
            else if (remainingTiles < 7)
            {
                if (remainingTiles == 0)
                {
                    GiveFeedback("There are no tiles left to swap with");
                }
                else
                {
                    GiveFeedback("Can only swap up to " + remainingTiles + " tiles, remaining tiles running out.");
                }
            }
        }

        internal string[] GetPlayers()
        {
            return Players;
        }

        internal int GetPlayerCount()
        {
            return Players.Length;
        }

        private void InitialisePlayers()
        {
            Players = new string[NumPlayers];
            for (int i = 0; i < NumPlayers; i++)
            {
                Players[i] = "Player" + (i + 1);
            }
        }

        internal void SwitchPlayer()
        {
            int nextPlayer = GetCurrentPlayerNum();
            if (nextPlayer == Players.Length)
            {
                nextPlayer = 0;
            }
            CurrentPlayer = Players[nextPlayer];
        }

        internal string GetPreviousPlayer()
        {
            int previousPlayerIndex = GetCurrentPlayerNum();
            if (previousPlayerIndex == 1)
            {
                previousPlayerIndex = Players.Length - 1;
            }
            else
            {
                previousPlayerIndex -= 2;
            }
            return Players[previousPlayerIndex];
        }

        private int GetCurrentPlayerNum()
        {
            return Convert.ToInt32(CurrentPlayer.Substring(CurrentPlayer.Length - 1));
        }

        internal void EndTurn()
        {
            VerifyAndCleanOldMsg();
            ToggleUserInput();
            PanelThisTurn = CommandPanelController.ActiveOptionsPanel;
            if (PanelThisTurn.Equals("TilePlacementOptions"))
            {
                TilePlacementEndTurnLogic();
            }
            else if (PanelThisTurn.Equals("SwapTileOptions"))
            {
                HasSwappedThisTurn = false;
            }
            SetAllInteractivity(PanelThisTurn, false);
            BoardTileSpawner.OnEndTurn();
            if (!GameFinished)
                NextTurn();
        }

        internal void ProcessExtensionLetters(string[] extensionLettersCord)
        {
            Tile tile;
            foreach (string rowCell in extensionLettersCord)
            {
                tile = BoardScanner.GetTileAtRowCell(rowCell);
                AddExtensionLetter(tile.PointValue);
            }
        }

        private void AddExtensionLetter(int letterValue)
        {
            PointTally.AddPrimaryMove(letterValue, CellBonusType.Cell);
        }

        internal void TilePlacementEndTurnLogic()
        {
            Selector.ClearTilePlacementHighlight();
            TileHolderSpawner.OnEndTurn();
            UndoManager.OnEndTurn();
            PlaceableCellCalculator.OnEndTurn();
            PointTally.OnEndTurn();
        }

        private void NextTurn()
        {
            if (TileRepository.IsRepositoryEmpty())
            {
                OnZeroRepositoryTiles();

            }
            // This coroutine calls switch player in controller after a set period of time (2 seconds)
            StartCoroutine(TileHolderSpawner.SwitchPlayer());
            if (!PanelThisTurn.Equals("PrimaryOptions"))
            {
                Invoke("SwitchToPrimaryPanel", 3);
            }
            Invoke("ResetButtonStates", 3);
            Invoke("ToggleUserInput", 3);
        }

        private void ToggleUserInput()
        {
            Selector.EndTurnPeriod = !Selector.EndTurnPeriod;
        }
        // Triggered at start of new turn if true
        private void OnZeroRepositoryTiles()
        {
            PrimaryOptions.OutOfTiles = true;
            PrimaryOptions.SetInteractableBtn("SwapTilesBtn", false);
            GiveFeedback("No more tiles left to swap with, can no longer Swap Tiles");
        }

        private void SwitchToPrimaryPanel()
        {
            CommandPanelController.SwitchPanel("Primary");
        }

        private void ResetButtonStates()
        {
            if (PanelThisTurn.Equals("TilePlacementOptions"))
            {
                TilePlacementOptions.SetDefaultButtonStates();
            }
            else if (PanelThisTurn.Equals("SwapTileOptions"))
            {
                SwapTileOptions.SetDefaultButtonStates();
            }
            else if (PanelThisTurn.Equals("PrimaryOptions"))
            {
                PrimaryOptions.SetDefaultButtonStates();
            }
        }

        internal void SetAllInteractivity(string mode, bool interactable)
        {
            if (mode.Equals("TilePlacementOptions"))
            {
                TilePlacementOptions.SetAllInteractivity(interactable);
            }
            else if (mode.Equals("SwapTileOptions"))
            {
                SwapTileOptions.SetAllInteractivity(interactable);
            }
            else if (mode.Equals("PrimaryOptions"))
            {
                PrimaryOptions.SetAllInteractivity(interactable);
            }
        }

        internal void SwitchOptionPanel(string type)
        {
            CommandPanelController.SwitchPanel(type);
        }

        internal bool CanHighlightCell()
        {
            if (CommandPanelController.ActiveOptionsPanel.Equals("TilePlacementOptions"))
            {
                return true;
            }
            return false;
        }

        internal bool CanHighlightTileHolderTile()
        {
            string activeOptionsPanel = CommandPanelController.ActiveOptionsPanel;
            if (activeOptionsPanel.Equals("TilePlacementOptions") || activeOptionsPanel.Equals("SwapTileOptions"))
            {
                return true;
            }
            return false;
        }

        internal void TurnOffHighlights()
        {
            string activeOptionsPanel = CommandPanelController.ActiveOptionsPanel;
            if (activeOptionsPanel.Equals("TilePlacementOptions"))
            {
                Selector.TurnOffHighlights();
            }
            else
            {
                Selector.ClearAllSwapHighlights();
            }
        }

        internal void SetTilePlacementBtnInteractable(bool interactable, string btnName)
        {
            TilePlacementOptions.SetInteractableBtn(btnName, interactable);
        }

        internal string GetActiveOptionsPanel()
        {
            return CommandPanelController.ActiveOptionsPanel;
        }

        internal void SetSwapConfirmBtn(bool highlightActive)
        {
            SwapTileOptions.SetInteractableBtn("ConfirmTileSwapBtn", highlightActive);
        }

        internal void SwitchLetters()
        {
            HasSwappedThisTurn = true;
            TileHolderSpawner.SwitchLetters(Selector.GetSwapHighlights());
            SwapTileOptions.SetInteractableBtn("ConfirmTileSwapBtn", false);
            SwapTileOptions.SetInteractableBtn("BackBtnSwapTiles", false);
            SwapTileOptions.SetInteractableBtn("FinishTurnBtnSwapTile", true);
        }

        internal void GiveFeedback(string feedback)
        {
            if (TimeStampMessage())
                feedback = AppendFeedback(feedback);
            if (currentClearRoutine != null)
                StopClearCoroutine();
            InformationDisplay.SetFeedbackStatement(feedback);
        }

        internal void GiveFeedback(string feedback, float secondsUntilClear = 10f)
        {
            if (TimeStampMessage())
            {
                feedback = AppendFeedback(feedback);
                if (secondsUntilClear < 5)
                    secondsUntilClear = 10f;
            }

            if (currentClearRoutine != null)
                StopClearCoroutine();
            InformationDisplay.SetFeedbackStatement(feedback);
            currentClearRoutine = StartCoroutine(ClearFeedback(secondsUntilClear));
        }

        private bool TimeStampMessage()
        {
            bool appendMessage = DateTime.Now.Subtract(LastMsgUpdateTime).Seconds < 2.5 ? true : false;
            LastMsgUpdateTime = DateTime.Now;
            return appendMessage;
        }

        private string AppendFeedback(string feedback)
        {
            string currentMessage = InformationDisplay.GetCurrentMessage();
            // if current feedback contains 2 messages, remove oldest one.
            if (currentMessage.Contains("\n"))
                currentMessage = currentMessage.Substring(currentMessage.IndexOf("\n") + 1);
            feedback = currentMessage + "\n" + feedback;
            return feedback;
        }

        private bool IsMsgOld()
        {
            if (DateTime.Now.Subtract(LastMsgUpdateTime).Seconds > 10)
                return true;
            return false;
        }

        private void VerifyAndCleanOldMsg()
        {
            if (IsMsgOld())
            {
                InformationDisplay.ClearFeedback();
            }
        }

        private void StopClearCoroutine()
        {
            StopCoroutine(currentClearRoutine);
            currentClearRoutine = null;
        }

        internal void OutOfTiles()
        {
            GameFinished = true;
            FinishGame(false);
        }

        internal int GetRemainingTileRepositoryTiles()
        {
            return TileRepository.GetRemainingTiles();
        }

        internal void UpdatePlayerPointsDisplay(string player, int points)
        {
            InformationDisplay.SetPlayerPoints(player, points);
        }

        internal void UpdateAllPlayerPointsDisplay(Dictionary<string, int> playerPoints)
        {
            foreach (KeyValuePair<string, int> kvp in playerPoints)
            {
                UpdatePlayerPointsDisplay(kvp.Key, kvp.Value);
            }
        }

        internal void SetPostBlankSelectionState()
        {
            TilePlacementOptions.SetInteractableBtn("UndoBtn", true);
            TilePlacementOptions.SetInteractableBtn("FinishTurnBtnTilePlacement", true);
            TilePlacementOptions.SetInteractableBtn("BackBtnTilePlacement", false);
        }

        internal void TallyMove(int letterValue, string multiplierType)
        {
            CellBonusType cellBonus = ParseCellBonus(multiplierType);
            PointTally.AddPrimaryMove(letterValue, cellBonus);
        }

        internal CellBonusType ParseCellBonus(string cellBonus)
        {
            return (CellBonusType)Enum.Parse(typeof(CellBonusType), cellBonus);
        }

        internal int GetLetterValue(string letter)
        {
            return TileRepository.GetLetterValue(letter);
        }

        internal void SetBlankSectionActive(bool active)
        {
            BlankTileSelection.SetActive(active);
        }

        internal void FinishGame(bool gameEndedByPass)
        {
            PointTally.DeductTileHolderValues(TileHolderSpawner.GetCurrentPlayerTiles(), gameEndedByPass);
            bool multipleWinners;
            string winners = PointTally.GetWinner(out multipleWinners);
            DeclareWinners(winners, multipleWinners);
            CommandPanelController.SwitchPanel("Finish");
        }

        internal void DeclareWinners(string winners, bool multipleWinners)
        {
            string winningMessage = "Congratulations to ";
            if (multipleWinners)
            {
                foreach (string winner in winners.Split('|'))
                {
                    winningMessage += GetFormattedPlayerName(winner) + " and ";
                }
                winningMessage = winningMessage.Substring(0, winningMessage.Length - 4);
            }
            else
            {
                winningMessage += GetFormattedPlayerName(winners);
            }
            GiveFeedback(winningMessage);
        }

        internal void UpdateConsecutivePasses(bool playerPassedTurn)
        {
            ConsecutivePasses = playerPassedTurn ? ++ConsecutivePasses : 0;
            if (ConsecutivePasses == EndGamePassCount)
            {
                GameFinished = true;
                GiveFeedback(GameName + " is finished, all players passed twice in a row");
                FinishGame(true);
            }
            int passedTurnsTillEnd = EndGamePassCount - ConsecutivePasses;
            if (passedTurnsTillEnd <= 2 && passedTurnsTillEnd > 0)
            {
                string[] feedback = new string[] { GameName + " will end if the next turn is passed", GameName + " will finish if the next two turns pass" };
                GiveFeedback(feedback[EndGamePassCount - ConsecutivePasses - 1]);
            }
        }

        internal string GetFormattedPlayerName(string requestedPlayer = "")
        {
            if (string.IsNullOrEmpty(requestedPlayer))
                requestedPlayer = currentPlayer;
            return FormattedPlayerNames[Convert.ToInt32(requestedPlayer.Substring(6)) - 1];
        }

        internal string[] GetFormattedPlayerNames()
        {
            return FormattedPlayerNames;
        }

        // only current player can update their name.
        internal void UpdateFormattedPlayerName(string newName)
        {
            FormattedPlayerNames[Convert.ToInt32(CurrentPlayer.Substring(CurrentPlayer.Length - 1)) - 1] = newName;
        }

        internal bool HasGameEnded()
        {
            return GameFinished;
        }

        internal void LaunchStartMenu()
        {
            Destroy(GameObject.FindGameObjectWithTag("Settings"));
            string scene = "Start";
            SceneManager.LoadScene(scene);
        }

        internal void SetLookUpButtonInteractable(bool interactive)
        {
            WordLookUp.SetLookUpButtonInteractable(interactive);
        }

        /*
         * // This setting doesn't work on mobile
        internal void SetApplicationBackgroundRun(bool runInBackground)
        {
            Application.runInBackground = runInBackground;
        }*/
    }
}

