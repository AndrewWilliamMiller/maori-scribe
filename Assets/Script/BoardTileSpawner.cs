﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    public class BoardTileSpawner : MonoBehaviour
    {
        internal UndoManager UndoManager;
        private GameObject CurrentBlankCell;
        internal Controller Controller;
        //private bool BlanksPlayedThisTurn = false;
        private struct BlankTilesToConvert
        {
            public GameObject Tile;
            public int PointValue;
        }
        private List<BlankTilesToConvert> BlankTiles = new List<BlankTilesToConvert>();

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            UndoManager = GameObject.FindGameObjectWithTag("Controller").GetComponent<UndoManager>();
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
        }

        internal void SpawnTile(Vector3 position, Tile tile)
        {
            GameObject boardTile = Instantiate((GameObject)Resources.Load("Prefab/BoardTile"));
            if (tile.Letter.Equals("Blank"))
                UpdateCurrentBlankCell(boardTile);
            boardTile.transform.position = position;
            // generating new Tile Instance so that we can make changes to the board tile without it affecting tileHolderTile (allows blank tiles to be undone)
            boardTile.GetComponent<TileHolderTile>().MyTile = new Tile(tile.Letter, tile.PointValue);
            AddTileLetterMat(boardTile, tile.Letter);
            UndoManager.AddBoardTile(boardTile);
        }

        internal void UpdateCurrentBlankCell(GameObject tile)
        {
            CurrentBlankCell = tile;
        }

        private void AddTileLetterMat(GameObject tile, string newLetter)
        {
            GameObject letter = tile.transform.GetChild(0).gameObject;
            letter.GetComponent<Renderer>().material = (Material)Resources.Load("Material/Letters/" + newLetter);
        }

        internal void AddBlankTileLetter(string letter)
        {
            AddBlankTileToConvert(letter);
            // updates the letter but leaves point value as 0 because blank tiles are worth that until the next turn
            CurrentBlankCell.GetComponent<TileHolderTile>().MyTile.SwitchLetter(letter, 0);
            AddTileLetterMat(CurrentBlankCell, letter);
        }

        internal void AddBlankTileToConvert(string letter)
        {
            BlankTilesToConvert blankTile = new BlankTilesToConvert();
            blankTile.PointValue = Controller.GetLetterValue(letter);
            blankTile.Tile = CurrentBlankCell;
            BlankTiles.Add(blankTile);
        }

        // updating all the played blank tiles to have a point value that corresponds to their letter as blank tiles
        // are only worth 0 on the turn they are played.
        internal void OnEndTurn()
        {
            if (BlankTiles.Count > 0)
            {
                foreach (BlankTilesToConvert blankTile in BlankTiles)
                {
                    blankTile.Tile.GetComponent<TileHolderTile>().MyTile.PointValue = blankTile.PointValue;
                }
                BlankTiles.Clear();
            }
        }

        internal void RemoveBlankTileToConvert(GameObject boardTile)
        {
            BlankTilesToConvert blankTileToRemove = new BlankTilesToConvert();
            foreach (BlankTilesToConvert blankTile in BlankTiles)
            {
                if (blankTile.Tile == boardTile)
                {
                    blankTileToRemove = blankTile;
                    break;
                }
            }
            BlankTiles.Remove(blankTileToRemove);
        }
    }

}
