﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class WordClearBtn : MonoBehaviour
    {

        internal Controller Controller;
        internal Button WordClearButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            WordClearButton = GetComponent<Button>();
            WordClearButton.onClick.AddListener(ClearWord);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void ClearWord()
        {
            Controller.PlayButtonClickSound();
            Controller.ClearWord();
        }
    }
}
