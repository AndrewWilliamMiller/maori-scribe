﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    // Doesn't inherit from monobehaviour so the new keyword can be used without warning, monobehaviour would be used if you
    // interact with this like a main component, but instead it's a property of another class (object composition).
    public class Tile
    {

        internal string Letter;
        internal int PointValue;

        public Tile (string letter, int pointValue)
        {
            Letter = letter;
            PointValue = pointValue;
        }

        internal void SwitchLetter(string letter, int pointValue)
        {
            Letter = letter;
            PointValue = pointValue;
        }
    }
}

