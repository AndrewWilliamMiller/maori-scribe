﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class SwapTileOptions : MonoBehaviour
    {
        CanvasGroup CanvasGroup;
        RectTransform Panel;
        Button ConfirmTileSwapBtn;
        Button BackBtn;
        Button FinishTurnBtnSwapTile;
        internal CommandPanelController CommandPanelController;
        Dictionary<string, Button> Buttons = new Dictionary<string, Button>();
        Controller Controller;

        private void Awake()
        {
            Initialise();
        }

        private void Initialise()
        {
            CommandPanelController = GameObject.Find("Main").GetComponent<CommandPanelController>();
            InitialisePanel();
            InitialiseButtons();
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
        }

        private void InitialiseButtons()
        {

            ConfirmTileSwapBtn = GameObject.FindGameObjectWithTag("ConfirmTileSwapBtn").GetComponent<Button>();
            BackBtn = GameObject.FindGameObjectWithTag("BackBtnSwapTiles").GetComponent<Button>();
            FinishTurnBtnSwapTile = GameObject.FindGameObjectWithTag("FinishTurnBtnSwapTile").GetComponent<Button>();
            Buttons.Add("ConfirmTileSwapBtn", ConfirmTileSwapBtn);
            Buttons.Add("BackBtnSwapTiles", BackBtn);
            Buttons.Add("FinishTurnBtnSwapTile", FinishTurnBtnSwapTile);
            SetDefaultButtonStates();
        }

        private void InitialisePanel()
        {
            Panel = GetComponent<RectTransform>();
            CanvasGroup = GetComponent<CanvasGroup>();
            CommandPanelController.HidePanel(CanvasGroup);
            CommandPanelController.FormatPanel(Panel);
        }

        internal void SetInteractableBtn(string name, bool interactable)
        {
            if (Buttons.ContainsKey(name))
            {
                Buttons[name].interactable = interactable;
            }
        }

        internal void SetDefaultButtonStates()
        {
            SetInteractableBtn("ConfirmTileSwapBtn", false);
            SetInteractableBtn("FinishTurnBtnSwapTile", false);
            SetInteractableBtn("BackBtnSwapTiles", true);
        }

        internal void SetAllInteractivity(bool interactable)
        {
            foreach (string buttonName in Buttons.Keys)
            {
                SetInteractableBtn(buttonName, interactable);
            }
        }

        private void OnEnable()
        {
            Controller.GiveFeedbackRemainingTilesForSwap();
        }
    }
}
