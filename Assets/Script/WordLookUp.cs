﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class WordLookUp : MonoBehaviour
    {
        private string PartialURL = @"https://maoridictionary.co.nz/search?idiom=&phrase=&proverb=&loan=&histLoanWords=&keywords=";
        private InputField SearchWordField;
        internal Controller Controller;
        Button WordSearchButton;
        Button WordClearButton;

        private void Awake()
        {
            Initialise();
        }

        private void Update()
        {
            if (SearchWordField.isFocused && Input.GetKeyDown(KeyCode.Return) && SearchWordField.text.Length > 0)
            {
                SearchWord();
            }
        }

        private void RemoveNewLineCharacter()
        {
            SearchWordField.text = SearchWordField.text.Substring(0, SearchWordField.text.Length - 1);
        }

        private void Initialise()
        {
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            SearchWordField = GameObject.FindGameObjectWithTag("SearchWordField").GetComponent<InputField>();
            WordSearchButton = GameObject.FindGameObjectWithTag("WordSearchBtn").GetComponent<Button>();
            WordClearButton = GameObject.FindGameObjectWithTag("WordClearBtn").GetComponent<Button>();
        }

        internal void SearchWord()
        {
            Controller.PlayButtonClickSound();
            string word = GetSearchWord();
            if (word.Equals("Exitxyzxyz"))
            {

                Controller.LaunchStartMenu();
            }
            else
            {
                Application.OpenURL(PartialURL + word);
            }
        }

        internal void ClearWord()
        {
            SearchWordField.text = "";
        }

        private string GetSearchWord()
        {
            return SearchWordField.text;
        }

        internal void SetLookUpButtonInteractable(bool interactive)
        {
            WordSearchButton.interactable = interactive;
            WordClearButton.interactable = interactive;
        }
    }
}

