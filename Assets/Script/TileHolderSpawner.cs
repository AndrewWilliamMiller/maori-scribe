﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Maori
{
    // Responsible for generating tiles on the tile holder
    public class TileHolderSpawner : MonoBehaviour
    {
        private float SpawnDelay = 0.25f;
        private Vector3[] SpawnLocations = new Vector3[7];
        //private GameObject[] currentTiles;
        Dictionary<string, GameObject[]> CurrentPlayerTiles = new Dictionary<string, GameObject[]>();
        internal TileRepository TileRepository;
        internal Controller Controller;
        private bool IsGeneratingTiles = false;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            TileRepository = GameObject.FindGameObjectWithTag("Controller").GetComponent<TileRepository>();
            Controller = GameObject.Find("Main").GetComponent<Controller>();
            InitialiseSpawnLocations();
            CreateCurrentTilesStructure();
            NotifyTileFill(isRefill : false, secondsUntilClear : 3.5f);
            StartCoroutine(GenerateTileHolderTiles());
        }

        private void CreateCurrentTilesStructure()
        {
            string[] players = Controller.GetPlayers();
            foreach (string player in players)
            {
                CurrentPlayerTiles.Add(player, new GameObject[7]);
            }
        }

        private void InitialiseSpawnLocations()
        {
            GameObject[] TileHolderSpawnPoints = GameObject.FindGameObjectsWithTag("TileHolderSpawnPoint");
            for (int i = 0; i < TileHolderSpawnPoints.Length; i++)
            {
                SpawnLocations[i] = TileHolderSpawnPoints[i].gameObject.transform.position;
            }
        }

        // make sure the current player when coroutine starts is consistently referened for the remainder of the coroutine
        // even if the current player switches when the coroutine is still running
        private IEnumerator GenerateTileHolderTiles(bool[] generationPoints = null)
        {
            string player = Controller.CurrentPlayer;
            IsGeneratingTiles = true;
            if (generationPoints == null)
            {
                generationPoints = Enumerable.Repeat(true, 7).ToArray();
            }
            for (int i = 0; i < generationPoints.Length; i++)
            {
                if(Controller.GetRemainingTileRepositoryTiles() == 0)
                {
                    break;
                }
                if (generationPoints[i])
                {
                    GameObject tileHolderTile = Instantiate((GameObject)Resources.Load("Prefab/TileHolderTile"));
                    CurrentPlayerTiles[player][i] = tileHolderTile;
                    tileHolderTile.GetComponent<TileHolderTile>().SpawnPoint = i;
                    SetRandomLetter(tileHolderTile);
                    tileHolderTile.transform.position = SpawnLocations[i];
                    yield return new WaitForSeconds(SpawnDelay);
                }
            }
            IsGeneratingTiles = false;
            TileRepository.ReturnTiles();
        }

        private void SetRandomLetter(GameObject tileHolderTile)
        {
            Tile randomTile = TileRepository.GetRandomTile();
            tileHolderTile.GetComponent<TileHolderTile>().MyTile = randomTile;
            AttachLetter(tileHolderTile, randomTile);
        }

        internal void AttachLetter(GameObject tileHolderTile, Tile randomTile)
        {
            GameObject letter = tileHolderTile.transform.GetChild(0).gameObject;
            letter.GetComponent<Renderer>().material = (Material)Resources.Load("Material/Letters/" + randomTile.Letter);
        }

        internal Dictionary<string, GameObject[]> GetCurrentPlayerTiles()
        {
            return CurrentPlayerTiles;
        }

        internal void SwitchLetters(GameObject[] highlightedTileHolderTiles)
        {
            bool[] spawnPoints = Enumerable.Repeat(false, 7).ToArray();
            TileHolderTile tileHolderTile;
            for (int i = 0; i < highlightedTileHolderTiles.Length; i++)
            {
                tileHolderTile = highlightedTileHolderTiles[i].GetComponent<TileHolderTile>();
                TileRepository.ReturnTile(tileHolderTile.MyTile);
                spawnPoints[highlightedTileHolderTiles[i].GetComponent<TileHolderTile>().SpawnPoint] = true;
                StartCoroutine(RemoveTileHolderTile(highlightedTileHolderTiles[i]));
            }

            StartCoroutine(GenerateTileHolderTiles(spawnPoints));
        }

        private IEnumerator EndTurnSpawnDelay(float delay)
        {
            return null;
        }

        private IEnumerator RemoveTileHolderTile(GameObject tileHolderTile)
        {
            Destroy(tileHolderTile.GetComponent<BoxCollider>());
            float desiredHeight = 5f;
            Vector3 newPosition = tileHolderTile.transform.position;
            while (tileHolderTile.transform.position.y < desiredHeight)
            {
                newPosition.y += 0.25f;
                tileHolderTile.transform.position = newPosition;
                yield return new WaitForSeconds(0.02f);
            }
            Destroy(tileHolderTile);
        }

        internal void OnEndTurn()
        {
            if (!OutOfTiles() && !Controller.HasGameEnded())
            {
                RefillTileHolder();
            }
            else
            {
                // User has used up all tiles and has no tiles to draw from, this is one of the end game conditions
                if(!Controller.HasGameEnded())
                    Controller.OutOfTiles();
            }
        }
        private void NotifyTileFill(bool isRefill)
        {
            string fill = isRefill ? "Refilling " : "Filling ";
            Controller.GiveFeedback(fill + Controller.GetFormattedPlayerName() + "'s tile holder");
        }

        private void NotifyTileFill(bool isRefill, float secondsUntilClear)
        {
            string fill = isRefill ? "Refilling " : "Filling ";
            Controller.GiveFeedback(fill + Controller.GetFormattedPlayerName() + "'s tile holder", secondsUntilClear);
        }

        private void RefillTileHolder()
        {
            NotifyTileFill(true, 3f);
            bool[] spawnPoints = Enumerable.Repeat(false, 7).ToArray();
            foreach (GameObject tileHolderTile in CurrentPlayerTiles[Controller.CurrentPlayer])
            {
                if (!(tileHolderTile == null) && !tileHolderTile.activeSelf)
                {
                    spawnPoints[tileHolderTile.GetComponent<TileHolderTile>().SpawnPoint] = true;
                    Destroy(tileHolderTile);
                }
            }
            StartCoroutine(GenerateTileHolderTiles(spawnPoints));
        }

        private bool OutOfTiles()
        {
            if(TileRepository.IsRepositoryEmpty())
            {
                bool noTilesLeft = true;
                foreach (GameObject tileHolderTile in CurrentPlayerTiles[Controller.CurrentPlayer])
                {
                    noTilesLeft = tileHolderTile == null || tileHolderTile.activeSelf == false ? true : false;
                    // If a non-null tileHolderTile is found, there are tiles remaining break and return false
                    if (!noTilesLeft)
                        break;
                }
                return noTilesLeft;
            }
            else
            {
                return false;
            }
        }

        internal IEnumerator SwitchPlayer()
        {
            // Wait for the end turn refill to finish before generating tiles for the next player
            while (IsGeneratingTiles)
            {
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(1.5f);
            string previousPlayer = Controller.CurrentPlayer;
            Controller.SwitchPlayer();
            string newPlayer = Controller.CurrentPlayer;
            SetPlayerTilesActive(previousPlayer, false);
            // If player hasn't had a turn yet, generate them a fresh set of tiles
            if (CurrentPlayerTiles[newPlayer][0] == null && !TileRepository.IsRepositoryEmpty())
            {
                NotifyTileFill(false, 3f);
                StartCoroutine(GenerateTileHolderTiles());
            }
            // If player has had a turn before, make their tiles active again
            else
            {
                SetPlayerTilesActive(newPlayer, true);
            }

        }

        private void SetPlayerTilesActive(string player, bool active)
        {
            foreach (GameObject tileHolderTile in CurrentPlayerTiles[player])
            {
                if(tileHolderTile != null)
                    tileHolderTile.SetActive(active);
            }
        }
    }
}
