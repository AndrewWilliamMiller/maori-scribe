﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class SwapTilesBtn : MonoBehaviour
    {
        internal Controller Controller;
        internal Button PlayTilesButton;

        void Start()
        {
            Initialise();
        }


        private void Initialise()
        {
            PlayTilesButton = GetComponent<Button>();
            PlayTilesButton.onClick.AddListener(LaunchSwapTilesInterface);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void LaunchSwapTilesInterface()
        {
            Controller.PlayButtonClickSound();
            Controller.SwitchOptionPanel("SwapTile");
        }
    }
}
