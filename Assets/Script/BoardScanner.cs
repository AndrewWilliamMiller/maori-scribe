﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    // Responsible for Detecting Cells on the board in order to gather information, such as the letter of a used cell
    public class BoardScanner : MonoBehaviour
    {
        internal Controller Controller;
        internal PlaceableCellCalculator PlaceableCellCalculator;
        private GameObject currentBlankCell;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GetComponent<Controller>();
            PlaceableCellCalculator = GetComponent<PlaceableCellCalculator>();
        }

        internal CellBonusType GetBonusType(string rowCell)
        {
            int[] rowCellNums = PlaceableCellCalculator.RowCellToNums(rowCell);
            return GetBonusType(rowCellNums[0], rowCellNums[1]);
        }

        private CellBonusType GetBonusType(int row, int cell)
        {
            Vector3 rayCastLaunchPosition = RowCellToWorldPos(row, cell);
            LayerMask layerMask = (1 << LayerMask.NameToLayer("Cell"));
            RaycastHit hit;
            Ray ray = new Ray(rayCastLaunchPosition, Vector3.down);
            Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
            return Controller.ParseCellBonus(hit.collider.tag);
        }

        internal Tile GetTileAtRowCell(string rowCell)
        {
            int[] rowCellNums = PlaceableCellCalculator.RowCellToNums(rowCell);
            Vector3 worldPos = RowCellToWorldPos(rowCellNums[0], rowCellNums[1]);
            return GetTileFromWorldPos(worldPos);
        }

        private Tile GetTileFromWorldPos(Vector3 worldPos)
        {
            Vector3 rayCastLaunchPosition = worldPos;
            LayerMask layerMask = (1 << LayerMask.NameToLayer("BoardTile"));
            RaycastHit hit;
            Ray ray = new Ray(rayCastLaunchPosition, Vector3.down);
            Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
            return hit.collider.gameObject.GetComponent<TileHolderTile>().MyTile;
        }

        private Vector3 RowCellToWorldPos(int row, int cell)
        {
            float x = CellToX(cell);
            float y = 1f;
            float z = row - 7;
            return new Vector3(x, y, z);
        }

        private float CellToX(int cell)
        {
            float x = 7 - cell;
            return x;
        }

        internal void UpdateCurrentBlankCell(GameObject tile)
        {
            currentBlankCell = tile;
        }
    }
}