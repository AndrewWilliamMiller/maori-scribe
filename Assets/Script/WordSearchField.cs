﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class WordSearchField : MonoBehaviour
    {
        private InputField SearchWordField;
        internal Controller Controller;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            SearchWordField = GameObject.FindGameObjectWithTag("SearchWordField").GetComponent<InputField>();
            SearchWordField.characterValidation = InputField.CharacterValidation.Name;
            SearchWordField.onValueChanged.AddListener(delegate { CheckWord(); });
            Controller.SetLookUpButtonInteractable(false);
        }

        public void CheckWord()
        {
            bool isWordEmpty = IsWordEmpty();
            Controller.SetLookUpButtonInteractable(!isWordEmpty);
        }

        private bool IsWordEmpty()
        {
            return SearchWordField.text.Length == 0;
        }
    }
}
