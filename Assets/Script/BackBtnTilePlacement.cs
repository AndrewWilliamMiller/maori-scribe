﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class BackBtnTilePlacement : MonoBehaviour
    {
        internal Controller Controller;
        internal Button BackBtn;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            BackBtn = GetComponent<Button>();
            BackBtn.onClick.AddListener(Back);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void Back()
        {
            Controller.PlayButtonClickSound();
            Controller.TurnOffHighlights();
            Controller.SwitchOptionPanel("Primary");
        }
    }
}

