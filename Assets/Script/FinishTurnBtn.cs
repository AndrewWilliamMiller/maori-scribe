﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    // Responsible for all 3 finish buttons which are located in: PrimaryOptions, TilePlacementOptions, and SwapTileOptions
    public class FinishTurnBtn : MonoBehaviour
    {
        internal Controller Controller;
        internal Button FinishBtn;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            FinishBtn = GetComponent<Button>();
            FinishBtn.onClick.AddListener(EndTurn);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void EndTurn()
        {
            Controller.PlayButtonClickSound();
            bool playerPassedTurn = name.Equals("PassTurnBtnPrimary") ? true : false;
            Controller.UpdateConsecutivePasses(playerPassedTurn);
            Controller.EndTurn();
        }
    }
}

