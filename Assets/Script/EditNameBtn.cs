﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class EditNameBtn : MonoBehaviour
    {
        internal Controller Controller;
        private Button EditNameButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            EditNameButton = GetComponent<Button>();
            EditNameButton.onClick.AddListener(EditName);
        }

        private void EditName()
        {
            Controller.PlayButtonClickSound();
            Controller.EditName();
        }
    }
}

