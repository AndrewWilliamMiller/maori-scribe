﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class StartBtn : MonoBehaviour
    {
        private Button StartButton;
        internal StartController StartController;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartButton = GetComponent<Button>();
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            StartButton.onClick.AddListener(LaunchGame);
        }

        private void LaunchGame()
        {
            StartController.PlayButtonClickSound();
            StartController.LaunchGame();
        }
    }
}

