﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class ExitHelpBtn : MonoBehaviour
    {
        internal StartController StartController;
        private Button ExitHelpButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            ExitHelpButton = GetComponent<Button>();
            ExitHelpButton.onClick.AddListener(ReturnToStart);
        }

        private void ReturnToStart()
        {
            StartController.PlayButtonClickSound();
            StartController.ExitHelp();
        }
    }
}

