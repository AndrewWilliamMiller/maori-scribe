﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class Manual : MonoBehaviour
    {
        internal StartController StartController;
        private GameObject HelpDisplay;
        private GameObject TableOfContents;
        private Image Page;
        private Button PreviousHelpButton;
        private Button NextHelpButton;
        private const int maxPage = 10;
        private int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set
            {
                currentPage = value;
                CheckNavBtnInteractivity();
            }
        }

        private void CheckNavBtnInteractivity()
        {
            if(currentPage == 1)
            {
                PreviousHelpButton.interactable = false;
                NextHelpButton.interactable = true;
            }
            else if(currentPage == maxPage)
            {
                PreviousHelpButton.interactable = true;
                NextHelpButton.interactable = false;
            }
            else
            {
                PreviousHelpButton.interactable = true;
                NextHelpButton.interactable = true;
            }
        }

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            // HelpDisplay wraps the Image to display pages, the nav buttons, return to table of contents button and return to start menu btn
            HelpDisplay = GameObject.FindGameObjectWithTag("HelpDisplay");
            Page = GameObject.FindGameObjectWithTag("ManualPage").GetComponent<Image>();
            PreviousHelpButton = GameObject.FindGameObjectWithTag("PreviousHelpBtn").GetComponent<Button>();
            PreviousHelpButton.onClick.AddListener(LoadPreviousHelp);
            NextHelpButton = GameObject.FindGameObjectWithTag("NextHelpBtn").GetComponent<Button>();
            NextHelpButton.onClick.AddListener(LoadNextHelp);
            TableOfContents = GameObject.FindGameObjectWithTag("TableOfContents");
            HideHelpDisplay();
            HideTableOfContents();

        }

        internal void LaunchHelp()
        {
            ShowTableOfContents();
        }

        internal void ReloadTableOfContents()
        {
            HideHelpDisplay();
            ShowTableOfContents();
        }

        private void ShowTableOfContents()
        {
            TableOfContents.SetActive(true);
        }

        internal void LoadPage(int pageNumber)
        {
            StartController.PlayButtonClickSound();
            CurrentPage = pageNumber;
            Page.sprite = Resources.Load<Sprite>("Image/Manual/page" + pageNumber);
        }

        internal void ExitHelp()
        {
            TableOfContents.SetActive(false);
            HelpDisplay.SetActive(false);
        }

        private void HideHelpDisplay()
        {
            HelpDisplay.SetActive(false);
        }

        private void HideTableOfContents()
        {
            TableOfContents.SetActive(false);
        }

        private void ShowHelpDisplay(int pageNumber)
        {
            HelpDisplay.SetActive(true);
            LoadPage(pageNumber);
        }

        
        internal void LoadPageFromTableOfContents(string pageToLoad)
        {
            CurrentPage = Convert.ToInt32(pageToLoad.Substring(4));
            HideTableOfContents();
            ShowHelpDisplay(CurrentPage);
        }

        private void LoadNextHelp()
        {
            LoadPage(++CurrentPage);
        }

        private void LoadPreviousHelp()
        {
            LoadPage(--CurrentPage);
        }
    }
}

