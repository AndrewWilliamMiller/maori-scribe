﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class ExitApplication : MonoBehaviour
    {
        Button ExitButton;
        StartController StartController;
        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            ExitButton = GameObject.FindGameObjectWithTag("ExitBtn").GetComponent<Button>();
            ExitButton.onClick.AddListener(Quit);
        }

        private void Quit()
        {
            StartController.PlayButtonClickSound();
            StartController.Quit();
        }
    }
}

