﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class ReturnToTableOfContentsBtn : MonoBehaviour
    {
        internal StartController StartController;
        private Button ReturnToTableOfContentsButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            ReturnToTableOfContentsButton = GetComponent<Button>();
            ReturnToTableOfContentsButton.onClick.AddListener(ReloadTableOfContents);
        }

        private void ReloadTableOfContents()
        {
            StartController.PlayButtonClickSound();
            StartController.ReloadTableOfContents();
        }
    }
}
