﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    // responsible for instantiating random words to be floated across a camera which projects onto a render texture for the start menu.
    // spawns 5 random words at a time, which forever float across the screen, get destroyed, and spawn again as a new word
    public class FloatingTileManipulator : MonoBehaviour
    {
        private System.Random Rand = new System.Random();
        private List<string> Words;
        // padding and lineHeight should add up to 2.6f
        private float LaneHeight = 2.1f;
        private float LanePadding = 0.5f;
        private float LetterSpacing = 0.03f;
        private float FloatingWordZ = 13.04f;
        private float FloatingWordX = -17f;
        private struct LaneYSpawnRange
        {
            public float Min;
            public float Max;
        }
        private Dictionary<string, LaneYSpawnRange> LaneYSpawnRanges = new Dictionary<string, LaneYSpawnRange>();
        // used words are transfered here to help prevent repeat occurances in word spawning 
        // (could still happen after discard is readded to words where last word played is first word played next round, but minor detail)
        private List<string> DiscardedWords = new List<string>();

        void Start()
        {
            Initialise();
        }

        private IEnumerator FloatWord(string lane)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(0, 2f));
            float movementSpeed;
            float randY;
            GameObject floatingWord;
            float destructionPointX = 15f;
            while (true)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0, 3f));
                randY = UnityEngine.Random.Range(LaneYSpawnRanges[lane].Min, LaneYSpawnRanges[lane].Max);
                floatingWord = SpawnFloatingTile(randY);
                movementSpeed = UnityEngine.Random.Range(1f, 2f);
                Vector3 targetCoordinates = floatingWord.transform.localPosition;
                targetCoordinates.x = destructionPointX;
                while (Vector3.Distance(floatingWord.transform.localPosition, targetCoordinates) > 0.05f)
                {
                    floatingWord.transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
                    yield return null;
                }
                Destroy(floatingWord);
                yield return null;
            }
        }

        private void Initialise()
        {
            InitialiseWords();
            InitialiseLaneYRanges();
            StartFloatingWords();
        }

        private void StartFloatingWords()
        {
            for (int i = 1; i < 6; i++)
            {
                StartCoroutine(FloatWord("lane" + i));
            }
        }

        // each word can spawn within it's designated lane, lanes have padding to prevent words from getting too close to each other and the top & bottom screen boundaries.
        private void InitialiseLaneYRanges()
        {
            LaneYSpawnRange laneYSpawnRange;
            laneYSpawnRange.Min = 4.4f + LaneHeight;
            laneYSpawnRange.Max = 6f + LaneHeight;
            for (int i = 0; i < 5; i++)
            {
                laneYSpawnRange.Max = laneYSpawnRange.Max - LaneHeight;
                laneYSpawnRange.Min = laneYSpawnRange.Min - LaneHeight;
                LaneYSpawnRanges.Add("lane" + (i + 1), laneYSpawnRange);
                laneYSpawnRange.Max = laneYSpawnRange.Max - LanePadding;
                laneYSpawnRange.Min = laneYSpawnRange.Min - LanePadding;
            }
        }

        private GameObject GenerateFloatingTile()
        {
            GameObject FloatingWord = Instantiate((GameObject)Resources.Load("Prefab/FloatingWord"));
            FloatingWord.transform.position = new Vector3(-11.66f, LaneYSpawnRanges["lane3"].Max, FloatingWordZ);
            return FloatingWord;
        }

        private void AttachWord(GameObject parent, float yPosition)
        {
            string[] randWord = GetRandomWord();
            GameObject boardTile;
            float xOffset = 0;
            Vector3 boardTileSpawnSpot = parent.transform.position;
            foreach (string character in randWord)
            {
                boardTile = Instantiate((GameObject)Resources.Load("Prefab/BoardTile"));
                AttachLetter(boardTile, character);
                InitialiseBoardTileTransform(boardTile, parent, ref xOffset, ref boardTileSpawnSpot);
            }
            AdjustFloatingWordPos(parent, randWord.Length, yPosition);
        }

        private void AdjustFloatingWordPos(GameObject floatingWord, int floatingWordLength, float yPosition)
        {
            Vector3 newPosition = floatingWord.transform.position;
            newPosition.y = yPosition;
            newPosition.x = -13 - floatingWordLength;
            floatingWord.transform.position = newPosition;
        }

        private void AttachLetter(GameObject boardTile, string letter)
        {
            Material character = (Material)Resources.Load("Material/Letters/" + letter);
            boardTile.transform.GetChild(0).gameObject.GetComponent<Renderer>().material = character;
        }

        private void InitialiseBoardTileTransform(GameObject boardTile, GameObject parent, ref float xOffset, ref Vector3 boardTileSpawnSpot)
        {
            boardTile.transform.parent = parent.transform;
            boardTile.transform.rotation = parent.transform.rotation;
            boardTileSpawnSpot.x += xOffset;
            boardTile.transform.position = boardTileSpawnSpot;
            if (xOffset == 0)
                xOffset += boardTile.transform.localScale.x + LetterSpacing;
        }

        private GameObject SpawnFloatingTile(float yPosition)
        {
            GameObject floatingWord = GenerateFloatingTile();
            AttachWord(floatingWord, yPosition);
            return floatingWord;
        }

        private string[] GetRandomWord()
        {
            string selectedWord = Words[Rand.Next(0, Words.Count - 1)];
            ShiftWord(selectedWord);
            string[] result;
            if (selectedWord.Contains("|"))
            {
                result = selectedWord.Split('|');
            }
            else
            {
                result = new string[selectedWord.Length];
                for (int i = 0; i < selectedWord.Length; i++)
                {
                    result[i] = selectedWord[i].ToString();
                }
            }
            return result;
        }

        private void ShiftWord(string word)
        {
            Words.Remove(word);
            DiscardedWords.Add(word);
            if (Words.Count == 0)
            {
                Words = new List<string>(DiscardedWords);
                DiscardedWords.Clear();
            }
        }

        // contains used to determine if a string.split operation is needed (required for words with WH and NG characters)
        // words to be floated across screen
        private void InitialiseWords()
        {
            Words = new List<string>(new string[] { "AROHA", "AWA", "HAKA", "H|A|NG|I", "HAPU", "HIKOI", "HUI", "ITI", "IWI", "KAI", "KARAKIA",
            "KAUMATUA", "KAURI", "KIWI", "KOHA", "MAHI", "MANA", "MANUHIRI", "MARAE", "M|A|U|NG|A", "MOA", "MOANA",
            "MOTU", "NUI", "PA", "PUKU", "POUNAMU", "TAIHOA", "TAMA", "TAMAHINE", "TAMARIKI", "TANE", "T|A|O|NG|A",
            "TAPU", "WAHINE", "WAI", "WAIATA", "WAKA", "PAKARI", "TONO", "PUKANA", "KARE", "AHI", "AKO", "ANA", "HAU", "HOA", "HOKO", "HUA", "H|U|NG|A", "K|A|R|A|NG|A",
            "KARAREHE", "KAU", "KAUPAPA", "KIRI", "KOINEI", "MAHANA", "MANU", "MARAMA", "MATAKU", "MATUA", "MAU", "MERE", "MIHI", "MOKO", "MOKOPUNA", "MONI",
            "MUA", "MURI", "MUTU", "NG|A|H|E|R|E", "NG|A|R|O", "NOHO", "ORA", "PAI", "PAPA", "PEA", "PUKAPUKA", "PUPURI", "PUTA", "REKA", "RERE", "R|I|NG|A", "TAMAITI",
            "TATAU", "TAUIRA", "TOHU", "WERA", "WAHO", "TITIRO", "T|A|NG|A|T|A", "R|A|NG|A|T|I|R|A", "HAEATA", "A|K|O|NG|A", "KAIAKO", "KAIMAHI", "KAIARAHI", "KAIHAUTU",
            "MANUKURA", "HAPORI", "HIKUAWA", "KOMANAWA", "KAUNUKU", "KOREPO", "KURA", "PUNA"});
        }

        internal void StopAll()
        {
            StopAllCoroutines();
            GameObject[] floatingWords = GameObject.FindGameObjectsWithTag("FloatingWord");
            foreach (GameObject floatingWord in floatingWords)
            {
                Destroy(floatingWord);
            }
        }

        internal void StartAll()
        {
            StartFloatingWords();
        }
    }
}

