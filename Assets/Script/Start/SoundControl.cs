﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class SoundControl : MonoBehaviour
    {
        Button SoundEfxToggleBtn;
        Button MusicBtn;
        StartController StartController;
        Image SoundEfxImage;
        Image MusicImage;

        private void Awake()
        {
            SoundEfxToggleBtn = GameObject.FindGameObjectWithTag("SoundEfxToggleBtn").GetComponent<Button>();
            SoundEfxImage = SoundEfxToggleBtn.GetComponent<Image>();
            MusicBtn = GameObject.FindGameObjectWithTag("MusicToggleBtn").GetComponent<Button>();
            MusicImage = MusicBtn.GetComponent<Image>();
        }

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            SoundEfxToggleBtn.onClick.AddListener(ToggleSoundEffects);
            MusicBtn.onClick.AddListener(ToggleMusic);
        }

        private void ToggleSoundEffects()
        {
            StartController.ToggleSoundEffects();
            ToggleSFXBtnAppearance();
        }

        private void ToggleMusic()
        {
            StartController.ToggleMusic();
            ToggleMusicBtnAppearance();
        }

        private void ToggleSFXBtnAppearance()
        {
            bool wantedState = SoundEfxImage.sprite.name.Equals("audioOn") ? false : true;
            SetActiveSFXBtnAppearance(wantedState);
        }

        internal void SetActiveSFXBtnAppearance(bool enabled)
        {
            string state = enabled ? "On" : "Off";
            SoundEfxImage.sprite = Resources.Load<Sprite>("Image/Start/audio" + state);
        }

        internal void SetActiveMusicBtnAppearance(bool enabled)
        {
            string state = enabled ? "On" : "Off";
            MusicImage.sprite = Resources.Load<Sprite>("Image/Start/music" + state);
        }

        private void ToggleMusicBtnAppearance()
        {
            bool wantedState = MusicImage.sprite.name.Equals("musicOn") ? false : true;
            SetActiveMusicBtnAppearance(wantedState);
        }
    }
}



