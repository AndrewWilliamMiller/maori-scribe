﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class TableOfContentsBtn : MonoBehaviour
    {
        internal StartController StartController;
        private Button TableOfContentsButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            TableOfContentsButton = GetComponent<Button>();
            TableOfContentsButton.onClick.AddListener(GoToHelpPage);
        }

        private void GoToHelpPage()
        {
            StartController.GoToHelpPage(tag);
        }

    }
}

