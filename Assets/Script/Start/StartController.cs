﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Maori
{
    public class StartController : MonoBehaviour
    {
        internal SoundControl SoundControl;
        // Sound Set in IDE
        private AudioSource BtnSoundEffect;
        private GameObject GameSettings;
        internal Settings Settings;
        internal Manual Manual;
        private GameObject StartOptions;
        FloatingTileManipulator FloatingTileManipulator;
        private AudioSource StartMusic;


        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartMusic = GetComponent<AudioSource>();
            SoundControl = GameObject.FindGameObjectWithTag("SoundControl").GetComponent<SoundControl>();
            BtnSoundEffect = GameObject.FindGameObjectWithTag("SoundEffectSource").GetComponent<AudioSource>();
            FloatingTileManipulator = GameObject.FindGameObjectWithTag("FloatingTileProjection").GetComponent<FloatingTileManipulator>();
            GameSettings = GameObject.FindGameObjectWithTag("Settings");
            Settings = GameSettings.GetComponent<Settings>();
            DontDestroyOnLoad(Settings);
            Manual = GameObject.FindGameObjectWithTag("Manual").GetComponent<Manual>();
            StartOptions = GameObject.FindGameObjectWithTag("StartOptions");
            // Delay allows for PlayerPrefs to load first
            // preventing dropDown value changed event from triggering which would play button click sound in responce to PlayerPrefs loading.
            Invoke("AddSoundToDropDownMenus", 0.1f);
        }

        internal void ToggleSoundEffects()
        {
            Settings.ToggleSoundEffects();
        }

        internal void ToggleMusic()
        {
            Settings.ToggleMusic();
        }

        internal void ToggleStartScreenMusic(bool enabled)
        {
            if (enabled)
            {
                // Delay prevents annoying sound repetitions should the music button be mashed.
                StartMusic.PlayDelayed(0.25f);
            }
            else
            {
                StartMusic.Stop();
            }
        }

        private void AddSoundToDropDownMenus()
        {
            Dropdown PlayerNumDropdown = GameObject.FindGameObjectWithTag("PlayerNumDropdown").GetComponent<Dropdown>();
            Dropdown BlanksDropdown = GameObject.FindGameObjectWithTag("BlanksDropdown").GetComponent<Dropdown>();
            PlayerNumDropdown.onValueChanged.AddListener(delegate { PlayButtonClickSound(); });
            BlanksDropdown.onValueChanged.AddListener(delegate { PlayButtonClickSound(); });
        }

        internal void GoToHelpPage(string pageToLoad)
        {
            Manual.LoadPageFromTableOfContents(pageToLoad);
        }

        internal void ReloadTableOfContents()
        {
            Manual.ReloadTableOfContents();
        }

        private void LoadMainScene()
        {
            string scene = "Main";
            SceneManager.LoadScene(scene);
        }

        internal void LaunchHelp()
        {
            HideStartOptions();
            Manual.LaunchHelp();

        }

        internal void LaunchGame()
        {
            Settings.LoadSettings();
            LoadMainScene();
        }

        private void HideStartOptions()
        {
            FloatingTileManipulator.StopAll();
            StartOptions.SetActive(false);
        }

        internal void ExitHelp()
        {
            Manual.ExitHelp();
            StartOptions.SetActive(true);
            FloatingTileManipulator.StartAll();
        }

        internal void PlayButtonClickSound()
        {
            if (Settings.SoundEffectsEnabled)
                BtnSoundEffect.Play();
        }

        internal void SetSoundEfxBtnAppearance(bool enabled)
        {
            SoundControl.SetActiveSFXBtnAppearance(enabled);
        }

        internal void SetMusicBtnAppearance(bool enabled)
        {
            SoundControl.SetActiveMusicBtnAppearance(enabled);
        }

        internal void Quit()
        {
            Application.Quit();
        }

        private void OnApplicationQuit()
        {
            Settings.SavePreferences();
        }
    }
}
