﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class TileTypeSelector : MonoBehaviour
    {

        private Texture[] woodTypes;
        private Material BoardTileMaterial;
        private Button LeftNavBtn;
        private Button RightNavBtn;
        private int selectedWoodType = 0;
        private Button ProjectedTileType;
        internal StartController StartController;

        public int SelectedWoodType
        {
            get { return selectedWoodType; }
            set
            {
                selectedWoodType = value;
                ChangeMaterialTexture();
            }
        }

        void Start()
        {
            Initialise();
            ChangeMaterialTexture();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            woodTypes = Resources.LoadAll<Texture>("Texture/Wood/");
            BoardTileMaterial = Resources.Load<Material>("Material/Wood/Wood1");
            ChangeMaterialTexture();
            InitialiseButtons();
        }

        private void InitialiseButtons()
        {
            ProjectedTileType = GameObject.FindGameObjectWithTag("ProjectedTileType").GetComponent<Button>();
            ProjectedTileType.onClick.AddListener(NextTexture);
            LeftNavBtn = GameObject.FindGameObjectWithTag("LeftTileSelectBtn").GetComponent<Button>();
            LeftNavBtn.onClick.AddListener(PreviousTexture);
            RightNavBtn = GameObject.FindGameObjectWithTag("RightTileSelectBtn").GetComponent<Button>();
            RightNavBtn.onClick.AddListener(NextTexture);
        }

        private void ChangeMaterialTexture()
        {
            BoardTileMaterial.mainTexture = woodTypes[selectedWoodType];
        }

        private void PreviousTexture()
        {
            StartController.PlayButtonClickSound();
            if (SelectedWoodType == 0)
            {
                SelectedWoodType = woodTypes.Length - 1;
            }
            else
            {
                SelectedWoodType--;
            }
        }

        private void NextTexture()
        {
            StartController.PlayButtonClickSound();
            if (selectedWoodType == woodTypes.Length - 1)
            {
                SelectedWoodType = 0;
            }
            else
            {
                SelectedWoodType++;
            }
        }
    }
}

