﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class Settings : MonoBehaviour
    {
        internal bool SoundEffectsEnabled = true;
        //internal bool MusicEnabled = true;
        private bool musicEnabled = true;

        public bool MusicEnabled
        {
            get { return musicEnabled; }
            set
            {
                musicEnabled = value;
                StartController.ToggleStartScreenMusic(musicEnabled);
            }
        }


        internal int NumPlayers;
        internal string BlankSetting;
        private Dropdown BlanksDropdown;
        private string PreferredBlanksKey = "PreferredBlanks";
        private string PreferredTileTypeKey = "PreferredTileType";
        private string PreferredSoundEffectsKey = "PreferredSoundEffects";
        private string PreferredMusicKey = "PreferredMusicState";
        internal TileTypeSelector TileTypeSelector;
        internal StartController StartController;
       
        void Start()
        {
            Initialise();
        }

        internal void ToggleSoundEffects()
        {
            SoundEffectsEnabled = !SoundEffectsEnabled;
        }

        internal void ToggleMusic()
        {
            MusicEnabled = !MusicEnabled;
        }

        private void Initialise()
        {
            BlanksDropdown = GameObject.FindGameObjectWithTag("BlanksDropdown").GetComponent<Dropdown>();
            TileTypeSelector = GameObject.FindGameObjectWithTag("TileTypeSection").GetComponent<TileTypeSelector>();
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            LoadPreferences();
        }

        private void LoadPreferences()
        {
            if (PlayerPrefs.HasKey(PreferredBlanksKey))
            {
                BlanksDropdown.value = PlayerPrefs.GetInt(PreferredBlanksKey);
            }
            if (PlayerPrefs.HasKey(PreferredTileTypeKey))
            {
                TileTypeSelector.SelectedWoodType = PlayerPrefs.GetInt(PreferredTileTypeKey);
            }
            if (PlayerPrefs.HasKey(PreferredSoundEffectsKey))
            {
                SoundEffectsEnabled = Convert.ToBoolean(PlayerPrefs.GetInt(PreferredSoundEffectsKey));
                StartController.SetSoundEfxBtnAppearance(SoundEffectsEnabled);
            }
            if (PlayerPrefs.HasKey(PreferredMusicKey))
            {
                MusicEnabled = Convert.ToBoolean(PlayerPrefs.GetInt(PreferredMusicKey));
                StartController.SetMusicBtnAppearance(MusicEnabled);
            }
        }

        internal void SavePreferences()
        {
            PlayerPrefs.SetInt(PreferredBlanksKey, BlanksDropdown.value);
            PlayerPrefs.SetInt(PreferredTileTypeKey, TileTypeSelector.SelectedWoodType);
            PlayerPrefs.SetInt(PreferredSoundEffectsKey, Convert.ToInt32(SoundEffectsEnabled));
            PlayerPrefs.SetInt(PreferredMusicKey, Convert.ToInt32(MusicEnabled));
            PlayerPrefs.Save();
        }

        internal void LoadSettings()
        {
            SavePreferences();
            LoadNumPlayers();
            LoadBlankSetting();
        }

        private void LoadNumPlayers()
        {
            Dictionary<string, int> numPlayerConversion = new Dictionary<string, int>();
            numPlayerConversion.Add("Two", 2);
            numPlayerConversion.Add("Three", 3);
            numPlayerConversion.Add("Four", 4);
            Text selectedPlayerCount = GameObject.FindGameObjectWithTag("SelectedPlayerCount").GetComponent<Text>();
            NumPlayers = numPlayerConversion[selectedPlayerCount.text];
        }

        private void LoadBlankSetting()
        {
            BlankSetting = BlanksDropdown.captionText.text;
        }

    }
}

