﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class HelpBtn : MonoBehaviour
    {
        internal StartController StartController;
        private Button HelpButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            StartController = GameObject.FindGameObjectWithTag("StartController").GetComponent<StartController>();
            HelpButton = GetComponent<Button>();
            HelpButton.onClick.AddListener(LaunchHelp);
        }

        private void LaunchHelp()
        {
            StartController.PlayButtonClickSound();
            StartController.LaunchHelp();
        }
    }

}
