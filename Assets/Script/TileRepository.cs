﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    /* Responsible for generation of all the tiles and keeping track of current tiles in the repository which are available for drawing */
    public class TileRepository : MonoBehaviour
    {
        private System.Random Rand = new System.Random();
        private List<Tile> Tiles = new List<Tile>();
        private Dictionary<string, int> LetterPointValues = new Dictionary<string, int>();
        Dictionary<string, int> TileRatios = new Dictionary<string, int>();
        internal List<Tile> TilesToReturn = new List<Tile>();

        private void Awake()
        {
            Initialise();
        }

        private void Initialise()
        {
            string[] letters = new[] { "A", "T", "I", "W", "O", "H", "U", "P", "E", "M", "K", "NG", "N", "WH", "R", "Blank" };
            InitialiseTileRatios(letters);
            InitialiseLetterPointValues(letters);
            GenerateTiles();
        }

        private void GenerateTiles()
        {
            foreach (KeyValuePair<string, int> kvp in TileRatios)
            {
                for (int i = 0; i < kvp.Value; i++)
                {
                    Tile tile = new Tile(kvp.Key, LetterPointValues[kvp.Key]);
                    Tiles.Add(tile);
                }
            }
        }

        // The returned tiles are added back to main tile list after new tiles are drawn in the swap, this prevents getting back
        // the tiles you swap out in the same swap
        internal void ReturnTile(Tile tileToReturn)
        {
            TilesToReturn.Add(tileToReturn);
        }

        private void InitialiseLetterPointValues(string[] letters)
        {
            int[] pointValues = new[] { 1, 2, 1, 2, 1, 3, 1, 3, 2, 3, 2, 4, 2, 5, 2, 0 };
            for (int i = 0; i < letters.Length; i++)
            {
                LetterPointValues.Add(letters[i], pointValues[i]);
            }
        }

        internal void ReturnTiles()
        {
            foreach (Tile tile in TilesToReturn)
            {
                Tiles.Add(tile);
            }
            TilesToReturn.Clear();
        }

        private void InitialiseTileRatios(string[] letters)
        {
            int[] quantities = GetLetterRatio();
            for (int i = 0; i < letters.Length; i++)
            {
                TileRatios.Add(letters[i], quantities[i]);
            }
        }

        internal int[] GetLetterRatio()
        {
            string blankSetting;
            try
            {
                blankSetting = GameObject.FindGameObjectWithTag("Settings").GetComponent<Settings>().BlankSetting;
            }
            catch
            {
                blankSetting = "Ultra High";
                //Debug.Log("Blank Setting defaulted to ultra high due to game not starting from start menu");
            }
            int[] quantities;
            switch (blankSetting)
            {
                case "High":
                    // for every 8-ish lose one - 3, 2, 3, 2, 3, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 - total amount deducted added to blanks
                    quantities = new[] { 22, 13, 22, 10, 22, 9, 18, 9, 13, 9, 10, 9, 10, 5, 13, 31 };
                    break;
                case "Very High":
                    // double the above changes
                    quantities = new[] { 19, 11, 19, 8, 19, 8, 16, 8, 11, 8, 8, 8, 8, 4, 11, 59 };
                    break;
                case "Ultra High":
                    // just under half total tiles
                    quantities = new[] { 13, 8, 13, 6, 13, 5, 10, 5, 8, 5, 6, 5, 6, 3, 8, 111 };
                    break;
                default:
                    quantities = new[] { 25, 15, 25, 12, 25, 10, 20, 10, 15, 10, 12, 10, 12, 6, 15, 3 };
                    break;
            }
            return quantities;
        }

        internal Tile GetRandomTile()
        {
            Tile randomTile = Tiles[Rand.Next(Tiles.Count)];
            Tiles.Remove(randomTile);
            return randomTile;
        }

        internal void AddTile(Tile tile)
        {
            Tiles.Add(tile);
        }

        internal int GetRemainingTiles()
        {
            return Tiles.Count;
        }

        internal bool IsRepositoryEmpty()
        {
            return Tiles.Count == 0; ;
        }

        internal int GetLetterValue(string letter)
        {
            return LetterPointValues[letter];
        }
    }
}
