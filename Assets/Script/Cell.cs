﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    public class Cell : MonoBehaviour
    {
        private GameObject maoriCell;

        public GameObject MaoriCell
        {
            get { return maoriCell; }
            set { maoriCell = value; }
        }

        private int cellNumber;

        public int CellNumber
        {
            get { return cellNumber; }
            set { cellNumber = value; }
        }

        private Tile myTile = null;

        public Tile MyTile
        {
            get { return myTile; }
            set { myTile = value; }
        }

        private bool isOccupied;

        public bool IsOccupied
        {
            get { return isOccupied; }
            set { isOccupied = value; }
        }

        private bool isPlaceable;

        public bool IsPlaceable
        {
            get { return isPlaceable; }
            set { isPlaceable = value; }
        }
    }
}

