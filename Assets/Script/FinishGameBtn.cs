﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class FinishGameBtn : MonoBehaviour
    {
        internal Controller Controller;
        internal Button GameFinishedButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            GameFinishedButton = GetComponent<Button>();
            GameFinishedButton.onClick.AddListener(LaunchStartMenu);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void LaunchStartMenu()
        {
            Controller.PlayButtonClickSound();
            Controller.LaunchStartMenu();
        }
    }
}