﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    public class Row : MonoBehaviour
    {
        private Cell[] MyCells = new Cell[15];

        void Start()
        {
            InitialiseCells();
        }

        private void InitialiseCells()
        {
           for (int i = 0; i < MyCells.Length; i++)
            {
                GameObject maoriCell = transform.GetChild(i).gameObject;
                maoriCell.AddComponent<Cell>();
                Cell cell = maoriCell.GetComponent<Cell>();
                cell.MaoriCell = maoriCell;
                cell.CellNumber = i;
            }
        }

        internal void SetIsPlaceable(bool isPlaceable, int cellNumber)
        {
            MyCells[cellNumber].IsPlaceable = isPlaceable;
        }

        internal void SetIsOccupied(bool isOccupied, int cellNumber)
        {
            MyCells[cellNumber].IsOccupied = isOccupied;
        }

    }
}

