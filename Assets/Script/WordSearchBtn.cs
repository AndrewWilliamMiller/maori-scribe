﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Maori
{
    public class WordSearchBtn : MonoBehaviour
    {
        internal Controller Controller;
        internal Button WordSearchButton;

        void Start()
        {
            Initialise();
        }


        private void Initialise()
        {
            WordSearchButton = GetComponent<Button>();
            WordSearchButton.onClick.AddListener(SearchWord);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        // button sound request in searchWord function of WordLookUp Class
        private void SearchWord()
        {
            Controller.SearchWord();
        }
    }
}

