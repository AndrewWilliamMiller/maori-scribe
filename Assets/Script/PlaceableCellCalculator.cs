﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Maori
{
    // Responsible for giving feedback on whether or not a Cell is able to be used for placement of a tile
    // Responsible for calculating words connecting to the played word for the purposes of point tallying.
    public class PlaceableCellCalculator : MonoBehaviour
    {
        // Contains information of placeable cells relative to preexisting placed tiles
        private List<string> TurnStartPlaceableCells = new List<string>();
        // Contains information of placeable cells relative to user placed tiles
        private List<string> PostStartPlaceableCells = new List<string>();
        private Stack<string> UsedCells = new Stack<string>();
        private string CurrentlySelectedRowCell = "";
        private string StartingCell = "Row7,Cell7";
        internal Controller Controller;
        // Contains placeAble cells Added to PostStartPlaceAble Cells, so when undoing, can just remove those entries from the list
        // Instead of reverse engineering logic to remove them
        private string FirstCell = "";
        private IEnumerable<string> UsedCellsThisTurn;
        private delegate string SearchUnusedCell(int row, int cell);
        private Dictionary<string, Delegate> DirectionalEmptyCellSearch = new Dictionary<string, Delegate>();
        private delegate string SearchAttachedCells(string rowCell);
        private Dictionary<string, Delegate> DirectionalCellSearch = new Dictionary<string, Delegate>();
        //private Dictionary<string, Delegate>
        private Stack<ChangesThisMove> UndoData = new Stack<ChangesThisMove>();
        private struct ChangesThisMove
        {
            public string placeableCellsAdded;
            public string placeableCellsRemoved;
        }
        private string BuildDirection = "";

        void Start()
        {
            Initialise();
        }


        private void Initialise()
        {
            TurnStartPlaceableCells.Add(StartingCell);
            InitialiseDirectionDelegates();
            Controller = GetComponent<Controller>();
        }

        private void InitialiseDirectionDelegates()
        {
            string[] directions = { "up", "right", "down", "left" };
            InitialiseEmptyDirectionalDelegates(directions);
            InitialiseDirectionalDelegates(directions);
        }

        private void InitialiseEmptyDirectionalDelegates(string[] directions)
        {
            SearchUnusedCell searchUp = FindUnusedUpperCell;
            SearchUnusedCell searchRight = FindUnusedRightCell;
            SearchUnusedCell searchDown = FindUnusedLowerCell;
            SearchUnusedCell searchLeft = FindUnusedLeftCell;
            DirectionalEmptyCellSearch.Add(directions[0], searchUp);
            DirectionalEmptyCellSearch.Add(directions[1], searchRight);
            DirectionalEmptyCellSearch.Add(directions[2], searchDown);
            DirectionalEmptyCellSearch.Add(directions[3], searchLeft);
        }

        private void InitialiseDirectionalDelegates(string[] directions)
        {
            SearchAttachedCells searchUp = FindUpperAttachedCells;
            SearchAttachedCells searchRight = FindRightAttachedCells;
            SearchAttachedCells searchDown = FindLowerAttachedCells;
            SearchAttachedCells searchLeft = FindLeftAttachedCells;
            DirectionalCellSearch.Add(directions[0], searchUp);
            DirectionalCellSearch.Add(directions[1], searchRight);
            DirectionalCellSearch.Add(directions[2], searchDown);
            DirectionalCellSearch.Add(directions[3], searchLeft);
        }

        internal void OnEndTurn()
        {
            string extensionLetters;
            string[] connectedWords = ManageConnectingLetters(out extensionLetters);
            ProcessConnectedLetterData(extensionLetters, connectedWords);
            if (UsedCellsThisTurn.Count() == 7)
                Controller.Grant7TileBonus();
            // Access Undo Data Before clearing to update information on 
            RefreshTurnStartPlaceableCells();
            UndoData.Clear();
            PostStartPlaceableCells.Clear();
        }

        private void RefreshTurnStartPlaceableCells()
        {
            int[] currentRowCell;
            string currentAdjacentCells;
            foreach (string usedCell in UsedCellsThisTurn)
            {
                currentRowCell = RowCellToNums(usedCell);
                currentAdjacentCells = GetAdjacentCells(currentRowCell[0], currentRowCell[1]);
                foreach (string rowCell in RowCellsToArray(currentAdjacentCells))
                {
                    if (!UsedCells.Contains(rowCell) && !TurnStartPlaceableCells.Contains(rowCell))
                    {
                        TurnStartPlaceableCells.Add(rowCell);
                    }
                }
                if (PostStartPlaceableCells.Contains(usedCell))
                {
                    PostStartPlaceableCells.Remove(usedCell);
                }
            }
        }

        private void ProcessConnectedLetterData(string extensionLetters, string[] connectedWords)
        {
            if (!string.IsNullOrEmpty(extensionLetters))
            {
                Controller.ProcessExtensionLetters(RowCellsToArray(extensionLetters));
            }
            string playedLetter = "";
            string modifiedWord = "";
            foreach (string word in connectedWords)
            {
                modifiedWord = RemovePlayedLetter(word, out playedLetter);
                Controller.ScoreConnectedWord(RowCellsToArray(modifiedWord), playedLetter);
            }
        }

        private string RemovePlayedLetter(string word, out string playedLetter)
        {
            playedLetter = "";
            string modifiedWord = "";
            foreach (string rowCell in RowCellsToArray(word))
            {
                if (!UsedCellsThisTurn.Contains(rowCell))
                {
                    modifiedWord = AttachRowCell(rowCell, modifiedWord);
                }
                else
                {
                    playedLetter = rowCell;
                }
            }
            return modifiedWord;
        }

        // Letters the played word connects to, used to help scoring
        private string[] ManageConnectingLetters(out string extensionLetters)
        {
            List<string> connectingWords = new List<string>();
            UsedCellsThisTurn = UsedCells.Reverse().Skip(Math.Max(0, UsedCells.Count - UndoData.Count));
            // Letters that extend onto an existing word
            extensionLetters = GetExtensionLetters();
            int[] currentRowCell;
            string verticallyScannedWord = "";
            string horizontallyScannedWord = "";
            string[] horizontallyAdjoiningCells;
            string[] verticallyAdjoiningCells;
            foreach (string usedCell in UsedCellsThisTurn)
            {
                currentRowCell = RowCellToNums(usedCell);
                horizontallyAdjoiningCells = GetHorizontallyAdjoiningUsedCells(currentRowCell[0], currentRowCell[1]);
                verticallyAdjoiningCells = GetVerticallyAdjoiningUsedCells(currentRowCell[0], currentRowCell[1]);
                horizontallyScannedWord = ScanWordFromCell(usedCell, horizontallyAdjoiningCells, extensionLetters, UsedCellsThisTurn);
                verticallyScannedWord = ScanWordFromCell(usedCell, verticallyAdjoiningCells, extensionLetters, UsedCellsThisTurn);
                if (!string.IsNullOrEmpty(horizontallyScannedWord))
                    connectingWords.Add(horizontallyScannedWord);
                if (!string.IsNullOrEmpty(verticallyScannedWord))
                    connectingWords.Add(verticallyScannedWord);
            }
            return connectingWords.ToArray();
        }

        private string ScanWordFromCell(string usedCell, string[] directionalAdjacentCells, string extensionLetters, IEnumerable<string> usedCellsThisTurn)
        {
            if (!CanScanAdjacentWord(directionalAdjacentCells, usedCellsThisTurn))
                return null;
            string scanDirection = "";
            string searchedCells = "";
            string scannedWord = "";
            scannedWord = AttachRowCell(usedCell, scannedWord);
            foreach (string rowCell in directionalAdjacentCells)
            {
                if (UsedCells.Contains(rowCell) && !usedCellsThisTurn.Contains(rowCell) && !extensionLetters.Contains(rowCell))
                {
                    if (!scannedWord.Contains(rowCell))
                        scannedWord = AttachRowCell(rowCell, scannedWord);
                    scanDirection = GetDirectionOfRelativeCell(rowCell, usedCell);
                    searchedCells = DirectionalCellSearch[scanDirection].DynamicInvoke(rowCell).ToString();
                    foreach (string searchedCell in RowCellsToArray(searchedCells))
                    {
                        if (!usedCellsThisTurn.Contains(searchedCell) && !scannedWord.Contains(searchedCell))
                        {
                            scannedWord = AttachRowCell(searchedCell, scannedWord);
                        }
                    }
                }
            }
            if (!scannedWord.Equals(usedCell))
            {
                return scannedWord;
            }
            else
            {
                return null;
            }
        }

        private bool CanScanAdjacentWord(string[] directionalAdjacentCells, IEnumerable<string> usedCellsThisTurn)
        {
            if (directionalAdjacentCells.Length == 0)
                return false;
            bool adjacentCellDetected = false;
            // Detect if word to scan is the primary word, if so return null because we don't want to add it
            foreach (string cell in directionalAdjacentCells)
            {
                if (string.IsNullOrEmpty(cell))
                {
                    continue;
                }
                else
                {
                    adjacentCellDetected = true;
                }
                if (usedCellsThisTurn.Contains(cell) || !adjacentCellDetected)
                {
                    return false;
                }
            }
            return true;
        }

        // Building off an existing word such as ING on TEST for TESTING, or just from word such as E on TEST to make EAST
        // Scan and return them all, so they can be calculated alongside the primary word
        private string GetExtensionLetters()
        {
            string firstCell = UsedCellsThisTurn.ElementAt(0);
            string secondCell = "";
            if (UsedCellsThisTurn.Count() > 1)
                secondCell = UsedCellsThisTurn.ElementAt(1);
            string extendedLetters = "";
            List<string> scanDirections = new List<string>();
            if (!string.IsNullOrEmpty(secondCell))
            {
                // Scan both directions because you can build both ways (up & down or left & right)
                scanDirections.Add(GetDirectionOfRelativeCell(firstCell, secondCell));
                scanDirections.Add(GetDirectionOfRelativeCell(secondCell, firstCell));
                //extendedLetters = DirectionalCellSearch[scanDirections].DynamicInvoke(firstCell).ToString();
                foreach (string scanDir in scanDirections)
                {
                    extendedLetters = AttachRowCell(DirectionalCellSearch[scanDir].DynamicInvoke(firstCell).ToString(), extendedLetters);
                }
                // Scan both directions from the first placed Cell to scan all letters in this word, now need to remove the used letters
                extendedLetters = RemoveUsedRowCellsThisTurn(extendedLetters, UsedCellsThisTurn);
            }
            else
            {
                string[] horizontallyAdjoiningCells;
                string[] verticallyAdjoiningCells;
                int[] currentRowCell = RowCellToNums(firstCell);
                horizontallyAdjoiningCells = GetHorizontallyAdjoiningUsedCells(currentRowCell[0], currentRowCell[1]);
                verticallyAdjoiningCells = GetVerticallyAdjoiningUsedCells(currentRowCell[0], currentRowCell[1]);
                foreach (string verticalCell in verticallyAdjoiningCells)
                {
                    if (UsedCells.Contains(verticalCell))
                    {
                        scanDirections.Add(GetDirectionOfRelativeCell(verticalCell, firstCell));
                    }
                }
                if (scanDirections.Count == 0)
                {
                    foreach (string horizontalCell in horizontallyAdjoiningCells)
                    {
                        if (UsedCells.Contains(horizontalCell))
                        {
                            scanDirections.Add(GetDirectionOfRelativeCell(horizontalCell, firstCell));
                        }
                    }
                }
                foreach (string scanDir in scanDirections)
                {
                    extendedLetters = AttachRowCell(DirectionalCellSearch[scanDir].DynamicInvoke(firstCell).ToString(), extendedLetters);
                }
            }
            return extendedLetters;
        }

        private string RemoveUsedRowCellsThisTurn(string extendedLetters, IEnumerable<string> usedRowCellsThisTurn)
        {
            string result = "";
            foreach (string rowCell in RowCellsToArray(extendedLetters))
            {
                if (!usedRowCellsThisTurn.Contains(rowCell))
                {
                    result = AttachRowCell(rowCell, result);
                }
            }
            return result;
        }

        internal void Undo()
        {
            // Reverting all placeableCell changes that were stored in this struct as string data
            UsedCells.Pop();
            ChangesThisMove changesThisMove = UndoData.Pop();
            // Any placeable cells added that turn need to be reverted, so removed from the PostStartPlaceableCells
            if (!string.IsNullOrEmpty(changesThisMove.placeableCellsAdded))
            {
                string[] placeableCellsToRemove = RowCellsToArray(changesThisMove.placeableCellsAdded);
                foreach (string unwantedPlaceableCell in placeableCellsToRemove)
                {
                    PostStartPlaceableCells.Remove(unwantedPlaceableCell);
                }
            }

            // Any placeable cells that were removed that turn, for example, the cell that was used up due to placement, needs to be
            // readded, so that it can be placed on again
            if (!string.IsNullOrEmpty(changesThisMove.placeableCellsRemoved))
            {
                string[] placeableCellsToAdd = RowCellsToArray(changesThisMove.placeableCellsRemoved);
                foreach (string wantedPlaceableCell in placeableCellsToAdd)
                {
                    PostStartPlaceableCells.Add(wantedPlaceableCell);
                }
            }
        }

        // returns false if the move is illegal, else it returns true and updates relevant data
        internal bool ValidateSelectedMove(string row, string cell)
        {
            bool result;
            string rowCell = FormatRowCellData(row, cell);
            if (UndoData.Count == 0)
            {
                result = VerifyFirstMove(rowCell);
            }
            else
            {
                result = VerifySubsequentMove(rowCell);
            }
            // only want to add move when placing it, not during highlight stage which might not become a placement
            if (result)
            {
                CurrentlySelectedRowCell = rowCell;
            }
            return result;
        }

        internal void AddMove()
        {
            // used cells updated, but is cell removed from placeableCells (it might not matter since used cells can't be raycasted)
            UsedCells.Push(CurrentlySelectedRowCell);
            UpdatePlaceableData(CurrentlySelectedRowCell);
        }

        private void UpdatePlaceableData(string rowCell)
        {
            int[] rowCellNums = RowCellToNums(rowCell);
            if (UndoData.Count == 0)
            {
                GenerateFirstMoveData(rowCellNums[0], rowCellNums[1]);
            }
            else if (UndoData.Count == 1)
            {
                GenerateSecondMoveData(rowCellNums[0], rowCellNums[1]);
            }
            else
            {
                GenerateLateMoveData(rowCellNums[0], rowCellNums[1]);
            }
        }

        private bool VerifySubsequentMove(string rowCell)
        {
            if (PostStartPlaceableCells.Contains(rowCell))
            {
                return true;
            }
            Controller.GiveFeedback("All tiles placed in a turn must connect to each other and be built in one direction (vertical or horizontal)");
            return false;
        }

        internal bool VerifyFirstMove(string rowCell)
        {
            if (TurnStartPlaceableCells.Contains(rowCell))
            {
                return true;
            }
            if (UsedCells.Count == 0)
            {
                Controller.GiveFeedback("The first tile needs to be placed on the star", 10f);
            }
            else
            {
                Controller.GiveFeedback("Can't place tile there. Tiles should be placed next to each other", 10f);
            }
            return false;
        }

        internal void GenerateFirstMoveData(int currentRow, int currentCell)
        {
            FirstCell = FormatRowCellData(currentRow, currentCell);
            string[] newPlaceableCells = RowCellsToArray(GetAdjacentCells(currentRow, currentCell));
            ChangesThisMove changesThisMove = GetNewChangesThisMove();
            int[] usedCell;
            string newPlaceableCell;
            foreach (string placeableCell in newPlaceableCells)
            {
                newPlaceableCell = "";
                if (!CellUsed(placeableCell))
                {
                    newPlaceableCell = placeableCell;
                }
                else
                {
                    usedCell = RowCellToNums(placeableCell);
                    newPlaceableCell = FindAdjacentDirectionalPlaceableCell(usedCell, currentRow, currentCell);
                }
                if (!string.IsNullOrEmpty(newPlaceableCell))
                {
                    PostStartPlaceableCells.Add(newPlaceableCell);
                    changesThisMove.placeableCellsAdded = AttachRowCell(newPlaceableCell, changesThisMove.placeableCellsAdded);
                }
            }
            UndoData.Push(changesThisMove);
        }


        // Searches for a the next available empty cell. offsetCell determines direction to search
        // by comparing it's location to the current row and cell
        // if it's above, it will search above, if to the right, will search right etc
        private string FindAdjacentDirectionalPlaceableCell(int[] offsetCell, int currentRow, int currentCell)
        {
            string buildDirection;
            string newPlaceableCell = "";
            buildDirection = GetDirectionOfRelativeCell(offsetCell, currentRow, currentCell);
            newPlaceableCell = DirectionalEmptyCellSearch[buildDirection].DynamicInvoke(offsetCell[0], offsetCell[1]).ToString();
            return newPlaceableCell;
        }

        private string GetDirectionOfRelativeCell(int[] relativeCell, int row, int cell)
        {
            string direction = "";
            string buildDirection = GetBuildDirection(relativeCell[0], relativeCell[1], row, cell);
            if (buildDirection.Equals("Horizontal"))
            {
                if (relativeCell[1] > cell)
                {
                    direction = "right";
                }
                else
                {
                    direction = "left";
                }
            }
            else if (buildDirection.Equals("Vertical"))
            {
                if (relativeCell[0] > row)
                {
                    direction = "down";
                }
                else
                {
                    direction = "up";
                }
            }
            return direction;
        }

        private string GetDirectionOfRelativeCell(string relativeCell, string rowCell)
        {
            int[] offsetCell = RowCellToNums(relativeCell);
            int[] primaryCell = RowCellToNums(rowCell);
            return GetDirectionOfRelativeCell(offsetCell, primaryCell[0], primaryCell[1]);
        }

        internal void GenerateSecondMoveData(int currentRow, int currentCell)
        {
            BuildDirection = DetermineBuildDirection(currentRow, currentCell);
            string[] newPlaceableCells = GetDirectionalNewPlaceableCells(currentRow, currentCell);
            ChangesThisMove changesThisMove = GetNewChangesThisMove();
            foreach (string rowCell in PostStartPlaceableCells)
            {
                // is the rowCell in postStartPlaceableCells NOT contained in newPlaceableCells
                if (!newPlaceableCells.Any(rowCell.Contains))
                {
                    // If you're not in newPlaceableCells, you're getting removed, as placeable cells are now resticted
                    // to the direction the user is building in (vertical or horizontal, after first move, user could potentially 
                    // build either way)
                    changesThisMove.placeableCellsRemoved = AttachRowCell(rowCell, changesThisMove.placeableCellsRemoved);
                }
            }
            // if the PostStartPlaceable cells did not contain this rowCell then it's new, add it as a new thing added to the list
            // if it's not new, (already there, don't want to say it's new this move, or it'll get undone early if this move is undone)
            List<string> validatedPlaceableCells = new List<string>();
            foreach (string rowCell in newPlaceableCells)
            {
                // newPlaceableCell generation now checks for CellUsed, verify this is working, then remove this if statement
                if (true/*!CellUsed(rowCell)*/)
                {
                    validatedPlaceableCells.Add(rowCell);
                    if (!PostStartPlaceableCells.Contains(rowCell))
                    {
                        changesThisMove.placeableCellsAdded = AttachRowCell(rowCell, changesThisMove.placeableCellsAdded);
                    }
                }
            }
            // All Current placeable cells will now be replaced with ones that are adjacent to the first placed cell of the right build direction
            PostStartPlaceableCells = validatedPlaceableCells;
            UndoData.Push(changesThisMove);
        }

        private void GenerateLateMoveData(int row, int cell)
        {
            string placedRowCell = FormatRowCellData(row, cell);
            ChangesThisMove changesThisMove = GetNewChangesThisMove();
            // PostStartPlaceableCells should contain rowCell, or it shouldn't have been able to have been placed here
            // but, defensive programming if statement
            if (PostStartPlaceableCells.Contains(placedRowCell))
            {
                PostStartPlaceableCells.Remove(placedRowCell);
                changesThisMove.placeableCellsRemoved = AttachRowCell(placedRowCell, changesThisMove.placeableCellsRemoved);
            }
            // might need to handle use case where this may be null, should be similar handling to generate second move data (in theory)
            string[] newPlaceableCells = GetDirectionalNewPlaceableCells(row, cell);
            foreach (string rowCell in newPlaceableCells)
            {
                // may not need this conditional here, need to deskcheck it.
                if (true/*!PostStartPlaceableCells.Contains(rowCell)*/)
                {
                    PostStartPlaceableCells.Add(rowCell);
                    changesThisMove.placeableCellsAdded = AttachRowCell(rowCell, changesThisMove.placeableCellsAdded);
                }
            }
            UndoData.Push(changesThisMove);
        }

        private string[] GetDirectionalNewPlaceableCells(int currentRow, int currentCell)
        {
            string[] newPlaceableCells = null;
            if (BuildDirection.Equals("Horizontal"))
            {
                newPlaceableCells = GetNewHorizontalCells(currentRow, currentCell);
            }
            else if (BuildDirection.Equals("Vertical"))
            {
                newPlaceableCells = GetNewVerticalCells(currentRow, currentCell);
            }
            return newPlaceableCells;
        }

        private string[] GetNewHorizontalCells(int currentRow, int currentCell)
        {
            string newLeftCell = FindUnusedLeftCell(currentRow, currentCell);
            string newRightCell = FindUnusedRightCell(currentRow, currentCell);
            List<string> newPlaceableCells = new List<string>();
            if (!string.IsNullOrEmpty(newLeftCell))
                newPlaceableCells.Add(newLeftCell);
            if (!string.IsNullOrEmpty(newRightCell))
                newPlaceableCells.Add(newRightCell);
            return newPlaceableCells.ToArray();
        }

        private string[] GetNewVerticalCells(int currentRow, int currentCell)
        {
            string newUpperCell = FindUnusedUpperCell(currentRow, currentCell);
            string newLowerCell = FindUnusedLowerCell(currentRow, currentCell);
            List<string> newPlaceableCells = new List<string>();
            if (!string.IsNullOrEmpty(newUpperCell))
                newPlaceableCells.Add(newUpperCell);
            if (!string.IsNullOrEmpty(newLowerCell))
                newPlaceableCells.Add(newLowerCell);
            return newPlaceableCells.ToArray();
        }

        private string FindUnusedLeftCell(int row, int cell)
        {
            string rowCell = "Row" + row + "," + "Cell";
            string newLeftCell = "";
            for (int i = cell - 1; i >= 0; i--)
            {
                if (!UsedCells.Contains(rowCell + i))
                {
                    newLeftCell = rowCell + i;
                    break;
                }
            }
            return newLeftCell;
        }

        private string FindUnusedRightCell(int row, int cell)
        {
            string rowCell = "Row" + row + "," + "Cell";
            string newRightCell = "";
            for (int i = cell + 1; i <= 14; i++)
            {
                if (!UsedCells.Contains(rowCell + i))
                {
                    newRightCell = rowCell + i;
                    break;
                }
            }
            return newRightCell;
        }

        private string FindUnusedUpperCell(int row, int cell)
        {
            string rowCell = "Row,Cell" + cell;
            string newUpperCell = "";
            for (int i = row - 1; i >= 0; i--)
            {
                if (!UsedCells.Contains(rowCell.Insert(3, Convert.ToString(i))))
                {
                    newUpperCell = rowCell.Insert(3, Convert.ToString(i));
                    break;
                }
            }
            return newUpperCell;
        }

        private string FindUnusedLowerCell(int row, int cell)
        {
            string rowCell = "Row,Cell" + cell;
            string newLowerCell = "";
            for (int i = row + 1; i <= 14; i++)
            {
                if (!UsedCells.Contains(rowCell.Insert(3, Convert.ToString(i))))
                {
                    newLowerCell = rowCell.Insert(3, Convert.ToString(i));
                    break;
                }
            }
            return newLowerCell;
        }

        private string FindLeftAttachedCells(string rowCell)
        {
            string foundLeftCells = "";
            int[] rowCellNums = RowCellToNums(rowCell);
            rowCell = "Row" + rowCellNums[0] + "," + "Cell";
            for (int i = rowCellNums[1] - 1; i >= 0; i--)
            {
                if (UsedCells.Contains(rowCell + i))
                {
                    foundLeftCells = AttachRowCell(rowCell + i, foundLeftCells);
                }
                else
                {
                    break;
                }
            }
            return foundLeftCells;
        }

        private string FindRightAttachedCells(string rowCell)
        {
            string foundRightCells = "";
            int[] rowCellNums = RowCellToNums(rowCell);
            rowCell = "Row" + rowCellNums[0] + "," + "Cell";
            for (int i = rowCellNums[1] + 1; i <= 14; i++)
            {
                if (UsedCells.Contains(rowCell + i))
                {
                    foundRightCells = AttachRowCell(rowCell + i, foundRightCells);
                }
                else
                {
                    break;
                }
            }
            return foundRightCells;
        }

        private string FindUpperAttachedCells(string rowCell)
        {
            string foundUpperCells = "";
            int[] rowCellNums = RowCellToNums(rowCell);
            rowCell = "Row,Cell" + rowCellNums[1];
            for (int i = rowCellNums[0] - 1; i >= 0; i--)
            {
                if (UsedCells.Contains(rowCell.Insert(3, Convert.ToString(i))))
                {
                    foundUpperCells = AttachRowCell(rowCell.Insert(3, Convert.ToString(i)), foundUpperCells);
                }
                else
                {
                    break;
                }
            }
            return foundUpperCells;
        }

        private string FindLowerAttachedCells(string rowCell)
        {
            string foundLowerCells = "";
            int[] rowCellNums = RowCellToNums(rowCell);
            rowCell = "Row,Cell" + rowCellNums[1];
            for (int i = rowCellNums[0] + 1; i <= 14; i++)
            {
                if (UsedCells.Contains(rowCell.Insert(3, Convert.ToString(i))))
                {
                    foundLowerCells = AttachRowCell(rowCell.Insert(3, Convert.ToString(i)), foundLowerCells);
                }
                else
                {
                    break;
                }
            }
            return foundLowerCells;
        }

        private string DetermineBuildDirection(int currentRow, int currentCell)
        {
            int[] previousRowCellNums = RowCellToNums(FirstCell);
            return (GetBuildDirection(previousRowCellNums[0], previousRowCellNums[1], currentRow, currentCell));

        }

        private string GetBuildDirection(int row1, int cell1, int row2, int cell2)
        {
            if (row1 == row2)
            {
                return "Horizontal";
            }
            else if (cell1 == cell2)
            {
                return "Vertical";
            }
            return null;
        }

        internal int[] RowCellToNums(string rowCell)
        {
            string[] slicedRowCell = rowCell.Split(',');
            int rowNumber = Convert.ToInt32(slicedRowCell[0].Substring(3));
            int cellNumber = Convert.ToInt32(slicedRowCell[1].Substring(4));
            return new[] { rowNumber, cellNumber };
        }

        private string FormatRowCellData(string row, string cell)
        {
            return row + "," + cell;
        }

        private string FormatRowCellData(int row, int cell)
        {
            return "Row" + row + "," + "Cell" + cell;
        }

        private string GetAdjacentCells(int row, int cell)
        {
            string result = GetVerticallyAdjoiningCells(row, cell);
            result = GetHorizontallyAdjoiningCells(row, cell, result);
            return result;
        }

        private string GetAdjacentCells(string rowCell)
        {
            int[] rowCellNums = RowCellToNums(rowCell);
            string result = GetVerticallyAdjoiningCells(rowCellNums[0], rowCellNums[1]);
            result = GetHorizontallyAdjoiningCells(rowCellNums[0], rowCellNums[1], result);
            return result;
        }

        private string GetVerticallyAdjoiningCells(int row, int cell, string appendTo = "")
        {
            string result = appendTo;
            // generate upper cell
            if (row > 0)
            {
                result = AttachRowCell(row - 1, cell, result);
            }
            // generate lower cell
            if (row < 14)
            {
                result = AttachRowCell(row + 1, cell, result);
            }
            return result;
        }

        private string GetHorizontallyAdjoiningCells(int row, int cell, string appendTo = "")
        {
            string result = appendTo;
            // generate left cell
            if (cell > 0)
            {
                result = AttachRowCell(row, cell - 1, result);
            }
            // generate right cell
            if (cell < 14)
            {
                result = AttachRowCell(row, cell + 1, result);
            }
            return result;
        }

        private string[] GetHorizontallyAdjoiningUsedCells(int row, int cell)
        {
            List<string> result = new List<string>();
            foreach (string adjoinedCell in RowCellsToArray(GetHorizontallyAdjoiningCells(row, cell)))
            {
                if (UsedCells.Contains(adjoinedCell))
                {
                    result.Add(adjoinedCell);
                }
            }
            return result.ToArray();
        }

        private string[] GetVerticallyAdjoiningUsedCells(int row, int cell)
        {
            List<string> result = new List<string>();
            foreach (string adjoinedCell in RowCellsToArray(GetVerticallyAdjoiningCells(row, cell)))
            {
                if (UsedCells.Contains(adjoinedCell))
                {
                    result.Add(adjoinedCell);
                }
            }
            return result.ToArray();
        }

        private string AttachRowCell(int row, int cell, string rowCells)
        {
            string rowCell = FormatRowCellData(row, cell);
            if (!string.IsNullOrEmpty(rowCells))
            {
                rowCells += "|";
            }
            rowCells += rowCell;
            return rowCells;
        }

        private string AttachRowCell(string rowCell, string rowCells)
        {
            if (!string.IsNullOrEmpty(rowCells))
            {
                rowCells += "|";
            }
            rowCells += rowCell;
            return rowCells;
        }

        private bool CellUsed(string rowCell)
        {
            return UsedCells.Contains(rowCell);
        }

        private string[] RowCellsToArray(string rowCells)
        {
            return rowCells.Split('|');
        }

        private ChangesThisMove GetNewChangesThisMove()
        {
            ChangesThisMove changesThisMove = new ChangesThisMove();
            changesThisMove.placeableCellsAdded = "";
            changesThisMove.placeableCellsAdded = "";
            return changesThisMove;
        }
    }
}
