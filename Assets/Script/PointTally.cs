﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Maori
{
    // Responsible for taking in tile placement data, calculating and storing points, and giving out point information on request
    public class PointTally : MonoBehaviour
    {
        internal Controller Controller;
        private Dictionary<string, int> PlayersPoints = new Dictionary<string, int>();

        private struct BonusMultiplier
        {
            public string MultiplierType;
            public int Multiplier;
        }

        private struct Move
        {
            public int LetterValue;
            public CellBonusType CellBonusType;
        }

        private Stack<Move> MovesThisTurn = new Stack<Move>();

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            InitialisePlayersPoints();
        }

        private void InitialisePlayersPoints()
        {
            for (int i = 1; i < Controller.GetNumPlayers()+1; i++)
            {
                PlayersPoints.Add("Player" + i, 0);
            }
        }

        internal void AddPrimaryMove(int letterValue, CellBonusType cellBonusType)
        {
            Move move = new Move();
            move.LetterValue = letterValue;
            move.CellBonusType = cellBonusType;
            MovesThisTurn.Push(move);
        }

        // If letter bonus, apply it to playedCellValue, if word, apply it to all. LetterValues are all existing letters 
        // so will have no bonuses other than word which would could only come from playedCellValue
        internal void ScoreConnectedWord(int[] letterValues, CellBonusType bonusOfPlayedCell, int playedLetterValue)
        {
            int score = 0;
            score += letterValues.Sum();
            if (!ResolveLetterBonus(ref score, bonusOfPlayedCell, playedLetterValue))
            {
                if (bonusOfPlayedCell == CellBonusType.Cell2W || bonusOfPlayedCell == CellBonusType.Cell3W)
                {
                    score *= (int)bonusOfPlayedCell / 10;
                }
            }
            PlayersPoints[Controller.CurrentPlayer] += score;
        }

        internal void Undo()
        {
            MovesThisTurn.Pop();
        }

        internal void OnEndTurn()
        {
            TallyCurrentTurn();
            MovesThisTurn.Clear();
        }

        private void TallyCurrentTurn()
        {
            int result = 0;
            List<int> multipliers = new List<int>();
            foreach (Move move in MovesThisTurn)
            {
                // If there's no letter bonus checks for word Bonus
                if (!ResolveLetterBonus(ref result, move.CellBonusType, move.LetterValue))
                {
                    if (move.CellBonusType == CellBonusType.Cell2W || move.CellBonusType == CellBonusType.Cell3W)
                        multipliers.Add((int)move.CellBonusType / 10);
                }
            }
            foreach (int multiplier in multipliers)
            {
                result *= multiplier;
            }
            UpdateScore(result);
        }

        private bool ResolveLetterBonus(ref int score, CellBonusType bonusType, int letterValue)
        {
            if (bonusType == CellBonusType.Cell2L || bonusType == CellBonusType.Cell3L)
            {
                score += letterValue * (int)bonusType;
                return true;
            }
            score += letterValue;
            return false;
        }

        private void UpdateScore(int pointsToAdd, string player = "")
        {
            if (string.IsNullOrEmpty(player))
                player = Controller.CurrentPlayer;
            PlayersPoints[player] += pointsToAdd;
            Controller.UpdatePlayerPointsDisplay(player, PlayersPoints[player]);
        }

        internal int GetPlayerPoints(string player)
        {
            return PlayersPoints[player];
        }

        internal void Grant7TileBonus()
        {
            PlayersPoints[Controller.CurrentPlayer] += 50;
            Controller.GiveFeedback("50 point bonus for 7 played tiles");
        }

        // The points lost at the end of the game, each player loses an amount of points equal to the sum of points in their
        // tile holder (if a player ends the game by emptying their tile holder and there is no tiles left to draw then they don't
        // get this penalty, but instead, gain points equal to the amount every other player lost.
        internal void DeductTileHolderValues(Dictionary<string, GameObject[]> currentPlayerTiles, bool gameEndedByPass)
        {
            int totalDeductedPoints = 0;
            string playerToBonus = "";
            if (!gameEndedByPass)
            {
                playerToBonus = Controller.CurrentPlayer;
                currentPlayerTiles.Remove(playerToBonus);
            }
            foreach (KeyValuePair<string, GameObject[]> kvp in currentPlayerTiles)
            {
                totalDeductedPoints += DeductTileHolderSumPoints(kvp.Key, kvp.Value);
            }
            if (!gameEndedByPass)
                PlayersPoints[playerToBonus] += totalDeductedPoints;
            Controller.UpdateAllPlayerPointsDisplay(PlayersPoints);
        }

        internal int DeductTileHolderSumPoints(string player, GameObject[] tileHolderTiles)
        {
            int pointsDeducted = 0;
            foreach (GameObject tileHolderTile in tileHolderTiles)
            {
                if(tileHolderTile != null)
                    pointsDeducted += tileHolderTile.GetComponent<TileHolderTile>().MyTile.PointValue;
            }
            PlayersPoints[player] -= pointsDeducted;
            return pointsDeducted;
        }

        internal string GetWinner(out bool multipleWinners)
        {
            int maxPoints = PlayersPoints.Values.Max();
            string winners = "";
            foreach (string player in PlayersPoints.Keys)
            {
                if (PlayersPoints[player] == maxPoints)
                    winners += player + "|";
            }
            winners = winners.Substring(0, winners.Length - 1);
            multipleWinners = winners.Contains("|") ? true : false;
            return winners;
        }
    }
}
