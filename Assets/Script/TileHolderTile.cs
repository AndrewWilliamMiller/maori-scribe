﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    public class TileHolderTile : MonoBehaviour
    {

        private Tile myTile;

        public Tile MyTile
        {
            get { return myTile; }
            set { myTile = value; }
        }

        private int spawnPoint;

        public int SpawnPoint
        {
            get { return spawnPoint; }
            set { spawnPoint = value; }
        }

        internal void Purge()
        {
            Destroy(gameObject);
        }

        internal void Hide()
        {
            gameObject.SetActive(false);
        }

        internal void Show()
        {
            gameObject.SetActive(true);
        }
    }
}

