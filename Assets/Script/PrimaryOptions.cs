﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class PrimaryOptions : MonoBehaviour
    {
        CanvasGroup CanvasGroup;
        RectTransform Panel;
        Button PlayTilesBtn;
        Button SwapTilesBtn;
        Button PassTurnBtnPrimary;
        Dictionary<string, Button> Buttons = new Dictionary<string, Button>();
        internal CommandPanelController CommandPanelController;
        private bool outOfTiles = false;

        public bool OutOfTiles
        {
            get { return outOfTiles; }
            set { outOfTiles = value; }
        }

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            CommandPanelController = GameObject.Find("Main").GetComponent<CommandPanelController>();
            InitialisePanel();
            InitialiseButtons();
            //StartCoroutine("FadeIn");
            CommandPanelController.FadeIn(CanvasGroup, 2f);
        }

        private void InitialiseButtons()
        {
            PlayTilesBtn = GameObject.FindGameObjectWithTag("PlayTilesBtn").GetComponent<Button>();
            SwapTilesBtn = GameObject.FindGameObjectWithTag("SwapTilesBtn").GetComponent<Button>();
            PassTurnBtnPrimary = GameObject.FindGameObjectWithTag("PassTurnBtnPrimary").GetComponent<Button>();
            Buttons.Add("PlayTilesBtn", PlayTilesBtn);
            Buttons.Add("SwapTilesBtn", SwapTilesBtn);
            Buttons.Add("PassTurnBtnPrimary", PassTurnBtnPrimary);
        }

        private void InitialisePanel()
        {
            Panel = GetComponent<RectTransform>();
            CanvasGroup = GetComponent<CanvasGroup>();
            CommandPanelController.HidePanel(CanvasGroup);
            CommandPanelController.FormatPanel(Panel);
        }

        internal void SetInteractableBtn(string name, bool interactable)
        {
            Buttons[name].interactable = interactable;
        }

        internal void SetAllInteractivity(bool interactable)
        {
            foreach (string buttonName in Buttons.Keys)
            {
                SetInteractableBtn(buttonName, interactable);
            }
        }

        internal void SetDefaultButtonStates()
        {
            SetInteractableBtn("PlayTilesBtn", true);
            SetInteractableBtn("SwapTilesBtn", !outOfTiles);
            SetInteractableBtn("PassTurnBtnPrimary", true);
        }
    }
}


