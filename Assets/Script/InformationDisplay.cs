﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Linq;

namespace Maori
{
    public class InformationDisplay : MonoBehaviour
    {
        internal Controller Controller;
        private Text CurrentPlayer;
        private Text FeedbackStatement;
        private Dictionary<string, Text> PlayerPoints = new Dictionary<string, Text>();
        private Dictionary<string, Text> PlayerPointLbls = new Dictionary<string, Text>();
        //Dictionary<string, string> PlayerPointLabels = new Dictionary<string, string>();
        private Dictionary<string, string> PlayerNameTranslation = new Dictionary<string, string>();
        private InputField NewNameInputField;
        private bool IsEditingName = false;
        private bool RecentEdit = false;
        private Image EditNameBtnImage;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            NewNameInputField = GameObject.FindGameObjectWithTag("NewNameInputField").GetComponent<InputField>();
            NewNameInputField.characterValidation = InputField.CharacterValidation.Alphanumeric;
            HideNewNameInputField();
            Controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
            CurrentPlayer = GameObject.FindGameObjectWithTag("CurrentPlayer").GetComponent<Text>();
            FeedbackStatement = GameObject.FindGameObjectWithTag("FeedbackStatement").GetComponent<Text>();
            EditNameBtnImage = GameObject.FindGameObjectWithTag("EditNameBtn").GetComponent<Button>().GetComponent<Image>();
            InitialisePlayerPointLbls();
            InitialisePlayerPoints();
            //InitialisePlayerPointLabels();
            InitialisePlayerNameTranslation();
            HideInactivePlayers();
        }

        private void InitialisePlayerPointLbls()
        {
            for (int i = 1; i <= Controller.GetNumPlayers(); i++)
            {
                PlayerPointLbls.Add("Player" + (i), GameObject.FindGameObjectWithTag("Player" + i + "PointLbl").GetComponent<Text>());
            }
        }

        private void InitialisePlayerNameTranslation()
        {
            string[] players = Controller.GetPlayers();
            string[] formattedPlayerName = Controller.GetFormattedPlayerNames();
            for (int i = 0; i < players.Length; i++)
            {
                PlayerNameTranslation.Add(players[i], formattedPlayerName[i]);
            }
        }

        // a players name can only be updated during their turn
        internal void UpdatePlayerName(string name)
        {
            string player = Controller.CurrentPlayer;
            if (!ProposedNewNameExists(name))
            {
                PlayerNameTranslation[player] = name;
                CurrentPlayer.text = name;
                PlayerPointLbls[player].text = name;
            }
            else
            {
                if (!name.ToLower().Equals(PlayerNameTranslation[player].ToLower()))
                    Controller.GiveFeedback(name + " is already in use.");
            }
            SetCurrentPlayer(player);

        }

        private void HideInactivePlayers()
        {
            int playerCount = Controller.GetPlayerCount();
            for (int i = playerCount + 1; i < 5; i++)
            {
                GameObject.FindGameObjectWithTag("Player" + i + "PointLbl").SetActive(false);
            }
        }

        private bool ProposedNewNameExists(string name)
        {
            name = name.ToLower();
            foreach (string existingName in PlayerNameTranslation.Values)
            {
                if (existingName.ToLower().Equals(name))
                    return true;
            }
            return false;
        }

        private void InitialisePlayerPoints()
        {
            for (int i = 1; i <= Controller.GetNumPlayers(); i++)
            {
                PlayerPoints.Add("Player" + (i), GameObject.FindGameObjectWithTag("Player" + i + "PointValue").GetComponent<Text>());
            }
        }

        internal void SetCurrentPlayer(string player)
        {
            CurrentPlayer.text = PlayerNameTranslation[player];
        }

        internal void SetPlayerPoints(string player, int points)
        {
            PlayerPoints[player].text = Convert.ToString(points);
        }

        internal void SetFeedbackStatement(string feedback)
        {
            feedback = PurgeDuplicates(feedback);
            FeedbackStatement.text = feedback;
        }

        private string PurgeDuplicates(string newFeedback)
        {
            if (!newFeedback.Contains('\n'))
            {
                return newFeedback;
            }
            string[] feedbackStatements = newFeedback.Split('\n');
            if(feedbackStatements.Length == feedbackStatements.Distinct().Count())
            {
                return newFeedback;
            }
            else
            {
                return feedbackStatements[feedbackStatements.Length - 1];
            }
        }

        internal void ClearFeedback()
        {
            SetFeedbackStatement("");
        }

        internal string GetCurrentMessage()
        {
            return FeedbackStatement.text;
        }

        internal void HideCurrentPlayerLbl()
        {
            CurrentPlayer.text = "";
        }

        internal void InitialiseUpdateName()
        {
            if (!RecentEdit)
            {
                IsEditingName = true;
                UpdateEditBtnImage();
                HideCurrentPlayerLbl();
                ShowNewNameInputField();
                SetNameEditFocus();
                StartCoroutine("WaitForNewName");
            }
        }

        internal void HideNewNameInputField()
        {
            NewNameInputField.gameObject.SetActive(false);
        }

        internal void ShowNewNameInputField()
        {
            NewNameInputField.gameObject.SetActive(true);
        }

        internal IEnumerator WaitForNewName()
        {
            SetRecentEdit(true);
            // have to delay a frame to set focus
            yield return new WaitForEndOfFrame();
            NewNameInputField.Select();
            while (NewNameInputField.isFocused)
            {
                yield return null;
            }
            if (NewNameInputField.text.Length > 0)
            {
                UpdatePlayerName(NewNameInputField.text);
                Controller.UpdateFormattedPlayerName(NewNameInputField.text);
            }
            else
            {
                SetCurrentPlayer(Controller.CurrentPlayer);
            }
            ClearNewNameInputInterface();
            Invoke("SetRecentEdit", 0.1f);
            yield return null;
        }

        private void ClearNewNameInputInterface()
        {
            IsEditingName = false;
            UpdateEditBtnImage();
            NewNameInputField.text = "";
            HideNewNameInputField();
        }

        private void SetRecentEdit()
        {
            RecentEdit = false;
        }

        private void SetRecentEdit(bool recentEdit)
        {
            RecentEdit = recentEdit;
        }

        private void SetNameEditFocus()
        {
            EventSystem.current.SetSelectedGameObject(NewNameInputField.gameObject);
            NewNameInputField.ActivateInputField();
            NewNameInputField.Select();
        }

        private void UpdateEditBtnImage()
        {
            string imageToSet = IsEditingName ? "Image/Start/editTick" : "Image/Start/pencilBtn";
            EditNameBtnImage.sprite = Resources.Load<Sprite>(imageToSet);
        }
    }
}

