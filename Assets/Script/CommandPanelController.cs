﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    public class CommandPanelController : MonoBehaviour
    {
        // Responsible for managing the different sets of panels a user will have active, which is based on the type of actions
        // the user will be currently doing, e.g. selecting main action, playing tiles, swapping tiles, and maybe passing turn
        // Additionally responsible for formatting it's panels

        private struct OptionComponents
        {
            public RectTransform Panel;
            public CanvasGroup CanvasGroup;
        }
        private Dictionary<string, OptionComponents> OptionPanels = new Dictionary<string, OptionComponents>();
        // Allows the Controller to check what mode it is, useful for controlling what the user is allowed to do, in particular, tile placement
        internal string ActiveOptionsPanel = "PrimaryOptions";
        internal Controller Controller;

        private void Awake()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GameObject.Find("Main").GetComponent<Controller>();
            InitialiseOptionPanels();
        }

        private void InitialiseOptionPanels()
        {
            OptionPanels.Add("PrimaryOptions", GetOptionComponents("PrimaryOptions"));
            OptionPanels.Add("TilePlacementOptions", GetOptionComponents("TilePlacementOptions"));
            OptionPanels.Add("SwapTileOptions", GetOptionComponents("SwapTileOptions"));
            OptionPanels.Add("FinishOptions", GetOptionComponents("FinishOptions"));
            OptionPanels["PrimaryOptions"].Panel.gameObject.SetActive(true);
        }

        private OptionComponents GetOptionComponents(string type)
        {
            OptionComponents optionComponents = new OptionComponents();
            GameObject options = GameObject.Find(type);
            optionComponents.Panel = options.GetComponent<RectTransform>();
            optionComponents.CanvasGroup = options.GetComponent<CanvasGroup>();
            options.SetActive(false);
            return optionComponents;
        }

        internal void FormatPanel(RectTransform Panel)
        {
            Vector3 panelPosition = Panel.position;
            Panel.position = new Vector3(panelPosition.x, Panel.rect.height * 0.55f, panelPosition.z);
        }

        internal void HidePanel(CanvasGroup canvasGroup)
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
        }

        internal void FadeIn(CanvasGroup canvasGroup, float delayOnInitialisation)
        {
            StartCoroutine(FadeInPanel(canvasGroup, delayOnInitialisation));
        }

        private IEnumerator FadeInPanel(CanvasGroup canvasGroup, float delayOnInitialisation = 0)
        {
            canvasGroup.gameObject.SetActive(true);
            if(delayOnInitialisation > 0)
                yield return new WaitForSeconds(delayOnInitialisation);
            canvasGroup.alpha = 0;
            while (canvasGroup.alpha < 1)
            {
                canvasGroup.alpha += Time.deltaTime*0.75f;
                yield return null;
            }
            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
        }

        private IEnumerator FadeOutPanel(CanvasGroup canvasGroup)
        {
            canvasGroup.interactable = false;
            canvasGroup.alpha = 1;
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= Time.deltaTime*0.75f;
                yield return null;
            }
            // Coroutine ended early sometimes with transparency up, this should stop it staying visible after coroutines
            canvasGroup.alpha = 0;
            canvasGroup.gameObject.SetActive(false);
        }

        internal void SwitchPanel(string panelType)
        {
            StartCoroutine(FadeOutPanel(OptionPanels[ActiveOptionsPanel].CanvasGroup));
            StartCoroutine(FadeInPanel(OptionPanels[panelType + "Options"].CanvasGroup));
            ActiveOptionsPanel = panelType + "Options";
        }
    }
}

