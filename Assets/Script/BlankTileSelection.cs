﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlankTileSelection : MonoBehaviour
{
    Light SpotLight;

    private void Awake()
    {
        Initialise();
    }

    private void Start()
    {
        Hide();
    }

    private void Initialise()
    {
        SpotLight = GameObject.FindGameObjectWithTag("BlankSectionHighlight").GetComponent<Light>();
        SpotLight.enabled = false;
    }

    internal void SetLightState(bool lightState)
    {
       SpotLight.enabled = lightState;
    }

    internal void Hide()
    {
        SetActiveState(false);
    }

    internal void Show()
    {
        SetActiveState(true);
    }

    private void SetActiveState(bool active)
    {
        gameObject.SetActive(active);
    }

    internal void SetActive(bool isActive)
    {
        SetLightState(isActive);
        SetActiveState(isActive);
    }
}
