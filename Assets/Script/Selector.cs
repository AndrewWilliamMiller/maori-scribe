﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Maori
{
    public class Selector : MonoBehaviour
    {
        private BoardTileSpawner BoardTileSpawner;
        internal Controller Controller;
        private GameObject[] Highlighters = new GameObject[7];
        private List<GameObject> HighlightedTileHolderTiles = new List<GameObject>();
        private GameObject CellHighlighter;
        private GameObject SelectedTileHolderTile = null;
        private GameObject SelectedCell = null;
        private float HighlighterOffset = 0.013546f;
        internal UndoManager UndoManager;
        internal PlaceableCellCalculator PlaceableCellCalculator;
        private bool SelectingLetter = false;
        private Dictionary<int, GameObject> ActiveSwapHighlights = new Dictionary<int, GameObject>();
        private string HighlightedTileHolderTileTag = "TileHolderTileHighlighted";
        private string chosenBlankLetter = "";
        private bool endTurnPeriod = false;

        public bool EndTurnPeriod
        {
            get { return endTurnPeriod; }
            set { endTurnPeriod = value; }
        }

        void Start()
        {
            Initialise();
        }
        private void Initialise()
        {
            BoardTileSpawner = GameObject.FindGameObjectWithTag("GridArray").GetComponent<BoardTileSpawner>();
            UndoManager = GameObject.FindGameObjectWithTag("Controller").GetComponent<UndoManager>();
            Controller = GameObject.Find("Main").GetComponent<Controller>();
            InitialiseHighlighters();
            PlaceableCellCalculator = GetComponent<PlaceableCellCalculator>();
        }

        private void InitialiseHighlighters()
        {
            for (int i = 0; i < 7; i++)
            {
                Highlighters[i] = Instantiate((GameObject)Resources.Load("Prefab/Highlighter"));
                Highlighters[i].SetActive(false);
            }
            CellHighlighter = Instantiate((GameObject)Resources.Load("Prefab/CellHighlighter"));
            CellHighlighter.SetActive(false);
        }

        void Update()
        {
            if (!endTurnPeriod && Input.GetMouseButtonDown(0) && !SelectingLetter)
            {
                Vector3 mousePosition = Input.mousePosition;
                bool successfulHit;
                RaycastHit hit = Raycast(mousePosition, out successfulHit);
                if (successfulHit)
                {
                    string activeOptionsPanel = Controller.GetActiveOptionsPanel();
                    if (activeOptionsPanel.Equals("TilePlacementOptions"))
                    {
                        TilePlacementRaycast(hit);
                    }
                    else if (!Controller.HasSwappedThisTurn && activeOptionsPanel.Equals("SwapTileOptions"))
                    {
                        SwapTileRaycast(hit);
                    }
                    else if(hit.collider.tag.Contains("Cell"))
                    {
                        Controller.GiveFeedback("Click the \"Play Tiles\" Button if you want to place tiles this turn.", 10f);
                    }
                }
            }
        }

        private void TilePlacementRaycast(RaycastHit hit)
        {
            if (Controller.CanHighlightTileHolderTile() && hit.transform.tag.Equals("TileHolderTile"))
            {
                HighlightTileHolderTile(hit.transform.position);
                SelectedTileHolderTile = hit.transform.gameObject;
                CheckForDualHighlights();
            }
            else if (Controller.CanHighlightCell() && hit.transform.tag.Contains("Cell"))
            {
                string row = hit.transform.parent.name;
                string cell = hit.transform.name;
                if (PlaceableCellCalculator.ValidateSelectedMove(row, cell))
                {
                    HighlightCell(hit.transform.gameObject.transform.position);
                    SelectedCell = hit.transform.gameObject;
                    CheckForDualHighlights();
                }
            }
        }

        private GameObject GetUnusedHighlighter()
        {
            foreach (GameObject highlighter in Highlighters)
            {
                if (!highlighter.activeSelf)
                {
                    return highlighter;
                }
            }
            Debug.Log("Couldn't find an inactive highlighter!");
            return null;
        }

        private void SwapTileRaycast(RaycastHit hit)
        {
            string tag = hit.transform.tag;
            CheckCellClickedInSwapMode(tag);
            if (tag.Equals("TileHolderTile") || tag.Equals(HighlightedTileHolderTileTag))
            {
                Vector3 tilePosition = hit.transform.position;
                int instanceID = hit.collider.GetInstanceID();
                // If position is not in active highlights, tile not highlighted yet
                if (!ActiveSwapHighlights.ContainsKey(instanceID) && tag.Equals("TileHolderTile") && !MaxHighlightsReached(giveFeedback: true))
                {
                    hit.transform.tag = HighlightedTileHolderTileTag;
                    GameObject availableHighlighter = GetUnusedHighlighter();
                    availableHighlighter.transform.position = new Vector3(tilePosition.x, tilePosition.y - HighlighterOffset, tilePosition.z);
                    availableHighlighter.SetActive(true);
                    ActiveSwapHighlights.Add(instanceID, availableHighlighter);
                    HighlightedTileHolderTiles.Add(hit.transform.gameObject);

                }
                // Otherwise, the object is already highlighted, remove the highlight to "toggle" it off
                else if (tag.Equals(HighlightedTileHolderTileTag))
                {
                    ActiveSwapHighlights[instanceID].SetActive(false);
                    ActiveSwapHighlights.Remove(instanceID);
                    hit.transform.tag = "TileHolderTile";
                    HighlightedTileHolderTiles.Remove(hit.transform.gameObject);
                }
                UpdateSwapConfirmBtnState();
            }
        }

        private void CheckCellClickedInSwapMode(string tagOfRaycastHit)
        {
            if (tagOfRaycastHit.Contains("Cell"))
            {
                Controller.GiveFeedback("Can't place tiles if you're swapping them.", 10f);
            }
        }

        private int GetNumActiveSwapHighlights()
        {
            int count = 0;
            foreach (GameObject highlight in ActiveSwapHighlights.Values)
            {
                if (highlight.activeSelf)
                    count++;
            }
            return count;
        }

        private int GetMaxSwapHighlights()
        {
            return Controller.GetRemainingTileRepositoryTiles();
        }

        private bool MaxHighlightsReached(bool giveFeedback)
        {
            bool result = false;
            int maxSwapHighlights = GetMaxSwapHighlights();
            if (GetNumActiveSwapHighlights() >= maxSwapHighlights)
            {
                result = true;
            }
            if (giveFeedback && result)
            {
                Controller.GiveFeedbackRemainingTilesForSwap();
            }
            return result;
        }

        private void UpdateSwapConfirmBtnState()
        {
            if (ActiveSwapHighlights.Count == 0)
            {
                Controller.SetSwapConfirmBtn(false);
            }
            else if (!Controller.HasSwappedThisTurn)
            {
                Controller.SetSwapConfirmBtn(true);
            }
        }

        private void HighlightTileHolderTile(Vector3 tilePosition)
        {
            Highlighters[0].transform.position = new Vector3(tilePosition.x, tilePosition.y - HighlighterOffset, tilePosition.z);
            Highlighters[0].SetActive(true);
        }

        internal void ClearAllSwapHighlights()
        {
            foreach (GameObject highlight in ActiveSwapHighlights.Values)
            {
                highlight.SetActive(false);
            }
            GameObject[] highlightedTileHolderTiles = GameObject.FindGameObjectsWithTag(HighlightedTileHolderTileTag);
            if (highlightedTileHolderTiles.Length > 0)
            {
                foreach (GameObject highlightedTileHolderTile in highlightedTileHolderTiles)
                {
                    highlightedTileHolderTile.tag = "TileHolderTile";
                }
            }
            ActiveSwapHighlights.Clear();
            UpdateSwapConfirmBtnState();
            HighlightedTileHolderTiles.Clear();
        }

        internal void ClearTilePlacementHighlight()
        {
            Highlighters[0].SetActive(false);
            CellHighlighter.SetActive(false);
            SelectedTileHolderTile = null;
            SelectedCell = null;
        }

        private void HighlightCell(Vector3 cellPosition)
        {
            CellHighlighter.transform.position = new Vector3(cellPosition.x, cellPosition.y + HighlighterOffset, cellPosition.z);
            CellHighlighter.SetActive(true);
        }

        private void CheckForDualHighlights()
        {
            if (SelectedCell != null && SelectedTileHolderTile != null)
            {
                PlaceableCellCalculator.AddMove();
                Tile tile = SelectedTileHolderTile.GetComponent<TileHolderTile>().MyTile;
                Controller.TallyMove(tile.PointValue, SelectedCell.tag);
                Vector3 position = SelectedCell.transform.position;
                BoardTileSpawner.SpawnTile(position, tile);
                if (tile.Letter.Equals("Blank"))
                {
                    BlankTilePlaced();
                }
                SelectedTileHolderTile.GetComponent<TileHolderTile>().Hide();
                UndoManager.AddTileHolderTile(SelectedTileHolderTile);
                TurnOffHighlights();
            }
        }

        private void BlankTilePlaced()
        {
            chosenBlankLetter = "";
            RequestBlankLetter();
            StartCoroutine("WaitForLetter");
        }

        private void RequestBlankLetter()
        {
            Controller.SetBlankSectionActive(true);
            SelectingLetter = true;
            Controller.GiveFeedback("Select a letter for this blank tile");
            Controller.SetAllInteractivity("TilePlacementOptions", false);
        }

        private IEnumerator WaitForLetter()
        {
            while (string.IsNullOrEmpty(chosenBlankLetter))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    bool successfulHit;
                    RaycastHit hit = Raycast(Input.mousePosition, out successfulHit);
                    CheckBlankSelection(hit);
                }
                yield return null;
            }
        }

        // consider adding layer mask
        private void CheckBlankSelection(RaycastHit hit)
        {
            if (hit.collider.tag.Equals("BlankSelectionTile"))
            {
                chosenBlankLetter = hit.collider.name.Substring(9);
                BoardTileSpawner.AddBlankTileLetter(chosenBlankLetter);
                Controller.GiveFeedback("You've Selected: " + chosenBlankLetter);
                Controller.SetPostBlankSelectionState();
                SelectingLetter = false;
                Controller.SetBlankSectionActive(false);
            }
        }

        internal GameObject[] GetSwapHighlights()
        {
            GameObject[] highlightedTileHolderTiles = HighlightedTileHolderTiles.ToArray();
            ClearAllSwapHighlights();
            return highlightedTileHolderTiles;
        }

        internal void TurnOffHighlights()
        {
            Highlighters[0].SetActive(false);
            CellHighlighter.SetActive(false);
            SelectedCell = null;
            SelectedTileHolderTile = null;
        }

        private RaycastHit Raycast(Vector3 mousePosition, out bool successfulHit)
        {
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            successfulHit = Physics.Raycast(ray, out hit);
            return hit;
        }
    }
}

