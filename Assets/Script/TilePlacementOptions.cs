﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class TilePlacementOptions : MonoBehaviour
    {

        CanvasGroup CanvasGroup;
        RectTransform Panel;
        Button UndoBtn;
        Button BackBtn;
        Button FinishTurnBtnTilePlacement;
        internal CommandPanelController CommandPanelController;
        Dictionary<string, Button> Buttons = new Dictionary<string, Button>();

        private void Awake()
        {
            Initialise();

        }

        private void Initialise()
        {
            CommandPanelController = GameObject.Find("Main").GetComponent<CommandPanelController>();
            InitialisePanel();
            IniitiliseButtons();
        }

        private void IniitiliseButtons()
        {

            UndoBtn = GameObject.FindGameObjectWithTag("UndoBtn").GetComponent<Button>();
            BackBtn = GameObject.FindGameObjectWithTag("BackBtnTilePlacement").GetComponent<Button>();
            FinishTurnBtnTilePlacement = GameObject.FindGameObjectWithTag("FinishTurnBtnTilePlacement").GetComponent<Button>();
            Buttons.Add("UndoBtn", UndoBtn);
            Buttons.Add("BackBtnTilePlacement", BackBtn);
            Buttons.Add("FinishTurnBtnTilePlacement", FinishTurnBtnTilePlacement);
            SetDefaultButtonStates();
        }

        private void InitialisePanel()
        {
            Panel = GetComponent<RectTransform>();
            CanvasGroup = GetComponent<CanvasGroup>();
            CommandPanelController.HidePanel(CanvasGroup);
            CommandPanelController.FormatPanel(Panel);
        }

        internal void SetInteractableBtn(string name, bool interactable)
        {
            Buttons[name].interactable = interactable;
        }

        internal void SetAllInteractivity(bool interactable)
        {
            foreach (string buttonName in Buttons.Keys)
            {
                SetInteractableBtn(buttonName, interactable);
            }
        }

        internal void SetDefaultButtonStates()
        {
            SetInteractableBtn("UndoBtn", false);
            SetInteractableBtn("FinishTurnBtnTilePlacement", false);
            SetInteractableBtn("BackBtnTilePlacement", true);
        }
    }
}
