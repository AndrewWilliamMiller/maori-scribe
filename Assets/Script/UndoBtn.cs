﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class UndoBtn : MonoBehaviour
    {

        internal Controller Controller;
        internal Button UndoButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            UndoButton = GetComponent<Button>();
            UndoButton.onClick.AddListener(Undo);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void Undo()
        {
            Controller.PlayButtonClickSound();
            Controller.Undo();
        }
    }
}

