﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maori
{
    // Responsible for managing the undo logic for 1 turn, undoing moves on previous turns is not required.
    public class UndoManager : MonoBehaviour
    {
        Stack<GameObject> BoardTilesPlaced = new Stack<GameObject>();
        Stack<GameObject> TileHolderTilesUsed = new Stack<GameObject>();
        Controller Controller;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        internal void AddBoardTile(GameObject boardTile)
        {
            BoardTilesPlaced.Push(boardTile);
            if (BoardTilesPlaced.Count == 1)
            {
                // make backbtn of placeable tile mode non interactable
                SetTilesPlaced(true);
            }
        }

        internal void Undo()
        {
            GameObject boardTilePlaced = BoardTilesPlaced.Pop();
            if (boardTilePlaced.GetComponent<TileHolderTile>().MyTile.PointValue == 0)
                Controller.RemoveBlankTileToConvert(boardTilePlaced);
            Destroy(boardTilePlaced);
            TileHolderTilesUsed.Pop().GetComponent<TileHolderTile>().Show();
            if (BoardTilesPlaced.Count == 0)
            {
                // make backbtn of placeable tile mode interactable again
                SetTilesPlaced(false);
            }
        }

        internal void AddTileHolderTile(GameObject tileHolderTile)
        {
            TileHolderTilesUsed.Push(tileHolderTile);
        }

        internal void OnEndTurn()
        {
            BoardTilesPlaced.Clear();
            TileHolderTilesUsed.Clear();
        }

        private void SetTilesPlaced(bool tilesPlacedThisTurn)
        {
            Controller.SetTilePlacementBtnInteractable(!tilesPlacedThisTurn, "BackBtnTilePlacement");
            Controller.SetTilePlacementBtnInteractable(tilesPlacedThisTurn, "UndoBtn");
            Controller.SetTilePlacementBtnInteractable(tilesPlacedThisTurn, "FinishTurnBtnTilePlacement");
        }
    }
}

