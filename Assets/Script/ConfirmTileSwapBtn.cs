﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class ConfirmTileSwapBtn : MonoBehaviour
    {

        internal Controller Controller;
        internal Button ConfirmBtn;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            ConfirmBtn = GetComponent<Button>();
            ConfirmBtn.onClick.AddListener(Confirm);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void Confirm()
        {
            Controller.PlayButtonClickSound();
            Controller.SwitchLetters();
        }
    }
}

