﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maori
{
    public class PlayTilesBtn : MonoBehaviour
    {

        internal Controller Controller;
        internal Button PlayTilesButton;

        void Start()
        {
            Initialise();
        }

        private void Initialise()
        {
            PlayTilesButton = GetComponent<Button>();
            PlayTilesButton.onClick.AddListener(LaunchPlayTileInterface);
            Controller = GameObject.Find("Main").GetComponent<Controller>();
        }

        private void LaunchPlayTileInterface()
        {
            Controller.PlayButtonClickSound();
            Controller.SwitchOptionPanel("TilePlacement");
        }
    }
}

